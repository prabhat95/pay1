package BaseClass;


	import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import Action.Action;
import Utility.Extent;
import io.github.bonigarcia.wdm.WebDriverManager;

	public class Baseclass {
		
		public static Properties prop;

		public static WebDriver driver;
		
		@BeforeSuite(alwaysRun = true)
		public void loadConfig() {
		
			//for report
			Extent.setExtent();
			DOMConfigurator.configure("log4j.xml");
			
			try {
				prop = new Properties();
				FileInputStream ip = new FileInputStream(
				System.getProperty("user.dir") + "/Configuration/Config.properties");
				//System.getProperty("\\Configuration\\config.properties"));
				prop.load(ip);
				
				System.out.println(ip);

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	   
		public void launchApp() { 
		
//			String downloadFolderPath = System.getProperty("user.dir")+"/Pay1/Downloaded_Pdf";
//       
			
	        Map<String, Object> prefs = new HashMap<>();
//	        prefs.put("download.default_directory", downloadFolderPath);
//	        
	        ChromeOptions options = new ChromeOptions();
	        options.addArguments("--disable-features=EnableDownloadProtection");
	        options.setAcceptInsecureCerts(true);
	        options.setExperimentalOption("prefs", prefs);
	        options.addArguments("--ignore-certificate-errors");
			options.addArguments("--no-sandbox"); 

			
			String browserName = prop.getProperty("browser");
//			String browserName = "Chrome";
			System.out.println("Launching in browser"+ " " +browserName);
			
			if (browserName.equalsIgnoreCase("Chrome")) {
				
				WebDriverManager.chromedriver().setup();
//				WebDriverManager.chromedriver().browserVersion("116.0.5845.187").setup();
				
				driver = new ChromeDriver(options);
				
			} else if (browserName.equalsIgnoreCase("FireFox")) {
				
				WebDriverManager.firefoxdriver().setup();
				driver = new FirefoxDriver();
			
				
			} else if (browserName.equalsIgnoreCase("IE")) {
				
				WebDriverManager.iedriver().setup();
				driver = new InternetExplorerDriver();
				
			}
			
			driver.manage().window().maximize();
			
			Action.implicitWait(driver , 10);
			Action.pageLoadTimeOut(driver , 30);
			
			driver.get(prop.getProperty("url"));	
			
		}
		
		public void launchGmail() {
			
			String browserName = prop.getProperty("browser");
			System.out.println("Launching in browser"+ " " +browserName);
			
			if (browserName.equalsIgnoreCase("Chrome")) {
				WebDriverManager.chromedriver().setup();
				// Set Browser to ThreadLocalMap
				driver = new ChromeDriver();
			} else if (browserName.equalsIgnoreCase("FireFox")) {
				WebDriverManager.firefoxdriver().setup();
				driver = new FirefoxDriver();
			} else if (browserName.equalsIgnoreCase("IE")) {
				WebDriverManager.iedriver().setup();
				driver = new InternetExplorerDriver();
			}
			
			driver.manage().window().maximize();
			Action.implicitWait(driver , 10);
			Action.pageLoadTimeOut(driver , 30);
			
			driver.get(prop.getProperty("gmailurl"));
			
			
			
		}
		
		
//		 String suiteName = ctx.getCurrentXmlTest().getSuite().getName();
//		 Log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>" + suiteName);
		
		
		@AfterSuite(alwaysRun= true)
		public void afterSuite() {
			Extent.endReport();
		}
		
		
	}

	

