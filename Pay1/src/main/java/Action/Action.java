package Action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestListener;
import org.testng.Reporter;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import BaseClass.Baseclass;
import Utility.Extent;
import Utility.ListenerClass;
import Utility.Log;

/**
 * @author user
 *
 */

public class Action extends Baseclass implements ITestListener {

	ExtentReports extent;
	static ExtentTest test;
	Extent manager;
	ListenerClass ls;

	public static String Recent_File_in_Folder(String dirPath) {

		 File dir = new File(dirPath);
		
		    File[] files = dir.listFiles();
		   
		    
		    if (files == null || files.length == 0) {
		        return null;
		    }

		    File lastModifiedFile = files[0];
		    for (int i = 1; i < files.length; i++) {
		       if (lastModifiedFile.lastModified() < files[i].lastModified()) {
		           lastModifiedFile = files[i];
		       }
		    }
		    return lastModifiedFile.getAbsolutePath();
		
	}

	public static void scrollByVisibilityOfElement(WebDriver driver, WebElement ele) {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", ele);

	}

	public static void TestLog(String str) {
		Reporter.log(str);
	}

	public static void screencapture() throws IOException {
		File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		File screenshotName = new File(
				"C:\\Users\\SANDEEP.CHAUHAN\\Downloads\\ScreenshotSelenium\\" + driver.getTitle() + ".png");
		FileUtils.copyFile(screenshotFile, screenshotName);
		Reporter.log(
				"<br> <img src='file:///" + screenshotName.getAbsolutePath() + "' height='400' width='400' /><br>");
	}

	static Workbook workbook;

	public static void writeExcelDataInputFile(String Inputpath) throws IOException {

		FileInputStream fis = new FileInputStream(Inputpath);

		workbook = new XSSFWorkbook(fis);

	}

	public static String Current_Date(String formate) {

		try {
			Calendar calendar = Calendar.getInstance();

			// Create a SimpleDateFormat object with your desired date format
			SimpleDateFormat dateFormat = new SimpleDateFormat(formate);

			// Format the date and time
			String formattedDate = dateFormat.format(calendar.getTime());

			System.out.println("Current Date and Time: " + formattedDate);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return formate;

	}

	public static void writeExcelDataOutputFile(String OutputPath) throws IOException {

		FileOutputStream out = new FileOutputStream(new File(OutputPath));

		workbook.write(out);
		out.close();

	}

	public static void click(WebDriver driver, WebElement ele) {

		Actions act = new Actions(driver);
		act.moveToElement(ele).click().build().perform();

	}

	public static boolean isWordPresent(String sentence, String word) {

		String[] s = sentence.split(" ");

		// To temporarily store each individual word
		for (String temp : s) {

			// Comparing the current word
			// with the word to be searched
			if (temp.compareTo(word) == 0) {
				return true;
			}
		}
		return false;
	}

	public static boolean isServicePresent(String sentence, String word) {

		String[] s = sentence.split(" ");

		// To temporarily store each individual word
		for (String temp : s) {

			// Comparing the current word
			// with the word to be searched
			if (temp.compareTo(word) == 0) {
				Log.info("Service Present");

				return true;
			}
		}
		Log.info("Service Not Present");
		return false;
	}

	public static boolean isSubjectPresent(String sentence, String word) {

		String[] s = sentence.split(" ");

		// To temporarily store each individual word
		for (String temp : s) {

			// Comparing the current word
			// with the word to be searched
			if (temp.compareTo(word) == 0) {
				Log.info("Subject Present");
				test = Extent.test.log(Status.PASS, "Subject Present");

				return true;
			}
		}
		Log.info("Subject Not Present");

		test = Extent.test.log(Status.FAIL, MarkupHelper.createLabel("Subject Not Present", ExtentColor.WHITE));

		return false;
	}

	public static boolean findElement(WebDriver driver, WebElement ele) {
		boolean flag = false;
		try {
			ele.isDisplayed();
			flag = true;
		} catch (Exception e) {
			// System.out.println("Location not found: "+locatorName);
			flag = false;
		} finally {
			if (flag) {
				Log.info("Successfully Found element at");

			} else {
				Log.info("Unable to locate element at");
			}
		}
		return flag;
	}

	public static boolean isDisplayed(WebDriver driver, WebElement ele) {
		boolean flag = false;
		flag = findElement(driver, ele);
		if (flag) {
			flag = ele.isDisplayed();
			if (flag) {
				Log.info("The element is Displayed");
			} else {
				Log.info("The element is not Displayed");

			}
		} else {

			Log.info("Not displayed");

		}
		return flag;
	}

	public boolean isSelected(WebDriver driver, WebElement ele) {
		boolean flag = false;
		flag = findElement(driver, ele);
		if (flag) {
			flag = ele.isSelected();
			if (flag) {
				Log.info("The element is Selected");
			} else {
				Log.info("The element is not Selected");
			}
		} else {
			Log.info("Not selected ");
		}
		return flag;
	}

	public boolean isEnabled(WebDriver driver, WebElement ele) {
		boolean flag = false;
		flag = findElement(driver, ele);
		if (flag) {
			flag = ele.isEnabled();
			if (flag) {
				Log.info("The element is Enabled");
			} else {
				Log.info("The element is not Enabled");
			}
		} else {
			Log.info("Not Enabled ");
		}
		return flag;
	}

	/**
	 * Type text at location
	 * 
	 * @param locatorName
	 * @param text
	 * @return - true/false
	 */
	public static boolean type(WebElement ele, String text) {
		boolean flag = false;
		try {
			flag = ele.isDisplayed();
			ele.clear();
			ele.sendKeys(text);

			Log.info("Entered text........... :" + text);
			flag = true;
		} catch (Exception e) {
			Log.info("Location Not found");
			flag = false;
		} finally {
			if (flag) {
				Log.info("Successfully entered value");
			} else {
				Log.info("Unable to enter value");
			}

		}
		return flag;
	}
	
	

	public boolean selectBySendkeys(String value, WebElement ele) {
		boolean flag = false;
		try {
			ele.sendKeys(value);
			flag = true;
			return true;
		} catch (Exception e) {

			return false;
		} finally {
			if (flag) {
				Log.info("Select value from the DropDown");
			} else {
				Log.info("Not Selected value from the DropDown");
				// throw new ElementNotFoundException("", "", "")
			}
		}
	}

	/**
	 * select value from DropDown by using selectByIndex
	 * 
	 * @param locator     : Action to be performed on element (Get it from Object
	 *                    repository)
	 * 
	 * @param index       : Index of value wish to select from dropdown list.
	 * 
	 * @param locatorName : Meaningful name to the element (Ex:Year Dropdown, items
	 *                    Listbox etc..)
	 * 
	 */
	public static boolean selectByIndex(WebElement element, int index) {
		boolean flag = false;
		try {
			Select s = new Select(element);
			s.selectByIndex(index);
			flag = true;
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			if (flag) {
				Log.info("Option selected by Index");
			} else {
				Log.info("Option not selected by Index");
			}
		}
	}

	/**
	 * select value from DD by using value
	 * 
	 * @param locator     : Action to be performed on element (Get it from Object
	 *                    repository)
	 * 
	 * @param value       : Value wish to select from dropdown list.
	 * 
	 * @param locatorName : Meaningful name to the element (Ex:Year Dropdown, items
	 *                    Listbox etc..)
	 */

	public boolean selectByValue(WebElement element, String value) {
		boolean flag = false;
		try {
			Select s = new Select(element);
			s.selectByValue(value);
			flag = true;
			return true;
		} catch (Exception e) {

			return false;
		} finally {
			if (flag) {
				Log.info("Option selected by Value");
			} else {
				Log.info("Option not selected by Value");
			}
		}
	}

	/**
	 * select value from DropDown by using selectByVisibleText
	 * 
	 * @param locator     : Action to be performed on element (Get it from Object
	 *                    repository)
	 * 
	 * @param visibletext : VisibleText wish to select from dropdown list.
	 * 
	 * @param locatorName : Meaningful name to the element (Ex:Year Dropdown, items
	 *                    Listbox etc..)
	 */

	public boolean selectByVisibleText(String visibletext, WebElement ele) {
		boolean flag = false;
		try {
			Select s = new Select(ele);
			s.selectByVisibleText(visibletext);
			flag = true;
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			if (flag) {
				Log.info("Option selected by VisibleText");
			} else {
				Log.info("Option not selected by VisibleText");
			}
		}
	}

	public boolean mouseHoverByJavaScript(WebElement ele) {
		boolean flag = false;
		try {
			WebElement mo = ele;
			String javaScript = "var evObj = document.createEvent('MouseEvents');"
					+ "evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);"
					+ "arguments[0].dispatchEvent(evObj);";
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript(javaScript, mo);
			flag = true;
			return true;
		}

		catch (Exception e) {

			return false;
		} finally {
			if (flag) {
				Log.info("MouseOver Action is performed");
			} else {
				Log.info("MouseOver Action is not performed");
			}
		}
	}

	public static boolean JSClick(WebDriver driver, WebElement ele) {
		boolean flag = false;
		try {
			// WebElement element = driver.findElement(locator);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", ele);
			// driver.executeAsyncScript("arguments[0].click();", element);

			flag = true;

		}

		catch (Exception e) {
			throw e;

		} finally {
			if (flag) {
				Log.info("Click Action is performed");
			} else if (!flag) {
				Log.info("Click Action is not performed");
			}
		}
		return flag;
	}

	public boolean switchToFrameByIndex(WebDriver driver, int index) {
		boolean flag = false;
		try {
			new WebDriverWait(driver, 20);
			driver.switchTo().frame(index);
			flag = true;
			return true;
		} catch (Exception e) {

			return false;
		} finally {
			if (flag) {
				Log.info("Frame with index \"" + index + "\" is selected");
			} else {
				Log.info("Frame with index \"" + index + "\" is not selected");
			}
		}
	}

	/**
	 * This method switch the to frame using frame ID.
	 * 
	 * @param idValue : Frame ID wish to switch
	 * 
	 */

	public boolean switchToFrameById(WebDriver driver, String idValue) {
		boolean flag = false;
		try {
			driver.switchTo().frame(idValue);
			flag = true;
			return true;
		} catch (Exception e) {

			e.printStackTrace();
			return false;
		} finally {
			if (flag) {
				Log.info("Frame with Id \"" + idValue + "\" is selected");
			} else {
				Log.info("Frame with Id \"" + idValue + "\" is not selected");
			}
		}
	}

	/**
	 * This method switch the to frame using frame Name.
	 * 
	 * @param nameValue : Frame Name wish to switch
	 * 
	 */

	public boolean switchToFrameByName(WebDriver driver, String nameValue) {
		boolean flag = false;
		try {
			driver.switchTo().frame(nameValue);
			flag = true;
			return true;
		} catch (Exception e) {

			return false;
		} finally {
			if (flag) {
				Log.info("Frame with Name \"" + nameValue + "\" is selected");
			} else if (!flag) {
				Log.info("Frame with Name \"" + nameValue + "\" is not selected");
			}
		}
	}

	public boolean switchToDefaultFrame(WebDriver driver) {
		boolean flag = false;
		try {
			driver.switchTo().defaultContent();
			flag = true;
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			if (flag) {
				// SuccessReport("SelectFrame ","Frame with Name is selected");
			} else if (!flag) {
				// failureReport("SelectFrame ","The Frame is not selected");
			}
		}
	}

	public void mouseOverElement(WebDriver driver, WebElement element) {
		boolean flag = false;
		try {
			new Actions(driver).moveToElement(element).build().perform();
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag) {
				Log.info(" MouserOver Action is performed on ");
			} else {
				Log.info("MouseOver action is not performed on");
			}
		}
	}

	public static boolean moveToElement(WebDriver driver, WebElement ele) {
		boolean flag = false;
		try {
			// WebElement element = driver.findElement(locator);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].scrollIntoView(true);", ele);
			Actions actions = new Actions(driver);
			// actions.moveToElement(driver.findElement(locator)).build().perform();
			actions.moveToElement(ele).build().perform();
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	public static boolean mouseover(WebDriver driver, WebElement ele) {
		boolean flag = false;
		try {
			new Actions(driver).moveToElement(ele).build().perform();
			flag = true;
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			/*
			 * if (flag) {
			 * SuccessReport("MouseOver ","MouserOver Action is performed on \""+locatorName
			 * +"\""); } else {
			 * failureReport("MouseOver","MouseOver action is not performed on \""
			 * +locatorName+"\""); }
			 */
		}
	}

	public boolean draggable(WebDriver driver, WebElement source, int x, int y) {
		boolean flag = false;
		try {
			new Actions(driver).dragAndDropBy(source, x, y).build().perform();
			Thread.sleep(5000);
			flag = true;
			return true;

		} catch (Exception e) {

			return false;

		} finally {
			if (flag) {
				Log.info("Draggable Action is performed on \"" + source + "\"");
			} else if (!flag) {
				Log.info("Draggable action is not performed on \"" + source + "\"");
			}
		}
	}

	public boolean draganddrop(WebDriver driver, WebElement source, WebElement target) {
		boolean flag = false;
		try {
			new Actions(driver).dragAndDrop(source, target).perform();
			flag = true;
			return true;
		} catch (Exception e) {

			return false;
		} finally {
			if (flag) {
				Log.info("DragAndDrop Action is performed");
			} else if (!flag) {
				Log.info("DragAndDrop Action is not performed");
			}
		}
	}

	public boolean slider(WebDriver driver, WebElement ele, int x, int y) {
		boolean flag = false;
		try {
			// new Actions(driver).dragAndDropBy(dragitem, 400, 1).build()
			// .perform();
			new Actions(driver).dragAndDropBy(ele, x, y).build().perform();// 150,0
			Thread.sleep(5000);
			flag = true;
			return true;
		} catch (Exception e) {

			return false;
		} finally {
			if (flag) {
				Log.info("Slider Action is performed");
			} else {
				Log.info("Slider Action is not performed");
			}
		}
	}

	public boolean rightclick(WebDriver driver, WebElement ele) {
		boolean flag = false;
		try {
			Actions clicker = new Actions(driver);
			clicker.contextClick(ele).perform();
			flag = true;
			return true;
			// driver.findElement(by1).sendKeys(Keys.DOWN);
		} catch (Exception e) {

			return false;
		} finally {
			if (flag) {
				Log.info("RightClick Action is performed");
			} else {
				Log.info("RightClick Action is not performed");
			}
		}
	}

	public boolean switchWindowByTitle(WebDriver driver, String windowTitle, int count) {
		boolean flag = false;
		try {
			Set<String> windowList = driver.getWindowHandles();

			String[] array = windowList.toArray(new String[0]);

			driver.switchTo().window(array[count - 1]);

			if (driver.getTitle().contains(windowTitle)) {
				flag = true;
			} else {
				flag = false;
			}
			return flag;
		} catch (Exception e) {
			// flag = true;
			return false;
		} finally {
			if (flag) {
				Log.info("Navigated to the window with title");
			} else {
				Log.info("The Window with title is not Selected");
			}
		}
	}

	public static boolean switchToNewWindow(WebDriver driver) {
		boolean flag = false;
		try {

			Set<String> s = driver.getWindowHandles();
			Object popup[] = s.toArray();
			driver.switchTo().window(popup[1].toString());
			flag = true;
			return flag;
		} catch (Exception e) {
			flag = false;
			return flag;
		} finally {
			if (flag) {
				Log.info("Window is Navigated with title");
			} else {
				Log.info("The Window with title: is not Selected");
			}
		}
	}

	public boolean switchToOldWindow(WebDriver driver) {
		boolean flag = false;
		try {

			Set<String> s = driver.getWindowHandles();
			Object popup[] = s.toArray();
			driver.switchTo().window(popup[0].toString());
			flag = true;
			return flag;
		} catch (Exception e) {
			flag = false;
			return flag;
		} finally {
			if (flag) {
				Log.info("Focus navigated to the window with title");
			} else {
				Log.info("The Window with title: is not Selected");
			}
		}
	}

	public int getColumncount(WebElement row) {
		List<WebElement> columns = row.findElements(By.tagName("td"));
		int a = columns.size();
		System.out.println(columns.size());
		for (WebElement column : columns) {
			Log.info(column.getText());
			Log.info("|");
		}
		return a;
	}

	public int getRowCount(WebElement table) {
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		int a = rows.size() - 1;
		return a;
	}

	/**
	 * Verify alert present or not
	 * 
	 * @return: Boolean (True: If alert preset, False: If no alert)
	 * 
	 */

	public boolean Alert(WebDriver driver) {
		boolean presentFlag = false;
		// Alert alert = null imported package by shubham
		Assert alert = null;

		try {
			// Check the presence of alert
			alert = (Assert) driver.switchTo().alert();
			// if present consume the alert and also did changes on this line
			((org.openqa.selenium.Alert) alert).accept();
			presentFlag = true;
		} catch (NoAlertPresentException ex) {
			// Alert present; set the flag

			// Alert not present
			ex.printStackTrace();
		} finally {
			if (!presentFlag) {
				Log.info("The Alert is handled successfully");
			} else {
				Log.info("There was no alert to handle");
			}
		}

		return presentFlag;
	}

	public boolean launchUrl(WebDriver driver, String url) {
		boolean flag = false;
		try {
			driver.navigate().to(url);
			flag = true;
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			if (flag) {
				Log.info("Successfully launched \"" + url + "\"");
			} else {
				Log.info("Failed to launch \"" + url + "\"");
			}
		}
	}

	public boolean isAlertPresent(WebDriver driver) {
		try {
			driver.switchTo().alert();
			return true;
		} // try
		catch (NoAlertPresentException Ex) {
			return false;
		} // catch
	}

	public String getTitle(WebDriver driver) {
		boolean flag = false;

		String text = driver.getTitle();
		if (flag) {
			Log.info("Title of the page is: \"" + text + "\"");
		}
		return text;
	}

	public static String getCurrentURL(WebDriver driver) {
		boolean flag = false;

		String text = driver.getCurrentUrl();
		if (flag) {
			Log.info("Current URL is: \"" + text + "\"");
		}
		return text;
	}

	public static boolean click1(WebElement locator, String locatorName) {
		boolean flag = false;
		try {
			locator.click();
			flag = true;
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			if (flag) {
				Log.info("Able to click on \"" + locatorName + "\"");
			} else {
				Log.info("Click Unable to click on \"" + locatorName + "\"");
			}
		}

	}

//	public static void fluentWait(WebDriver driver,WebElement element, int timeOut) {
//	    Wait<WebDriver> wait = null;
//	    try {
//	        wait = new FluentWait<WebDriver>((WebDriver) driver)
//	        		.withTimeout(20, int)
//	        	    .pollingEvery(Duration.ofSeconds(2))
//	        	    .ignoring(Exception.class);
//	        wait.until(ExpectedConditions.visibilityOf(element));
//	        element.click();
//	    }catch(Exception e) {
//	    }
//	}

	public static void implicitWait(WebDriver driver, int timeOut) {

		driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
	}

	public void explicitWait(WebDriver driver, WebElement element, int timeOut) {
		WebDriverWait wait = new WebDriverWait(driver, timeOut);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public static void pageLoadTimeOut(WebDriver driver, int timeOut) {
		driver.manage().timeouts().pageLoadTimeout(timeOut, TimeUnit.SECONDS);
	}

	// ******************************************************************************************************************
	public static String getText(WebElement element) {
		boolean flag = false;

		String text = element.getText();
		if (flag) {
			Log.info("Current Entered text is: \"" + text + "\"");
		}
		return text;
	}

	public static void hoverElement(WebElement mainService) {

		Actions actions = new Actions(driver);
		actions.moveToElement(mainService).perform();
		String mainserviceText = mainService.getText();
		Log.info("Hovered Element is " + mainserviceText);

	}
	
	
	public static String waitForVisibleAndGetText(WebDriver driver, WebElement element) {
		try {
			Thread.sleep(1000);
			Log.info("Waiting for element visibility");
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.visibilityOf(element));
			
		

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String text = element.getText();
		System.out.println("Text Displayed : "+ text);
		return text;
	}

	public static void waitForVisible(WebDriver driver, WebElement element) {
		try {
			Thread.sleep(1000);
			Log.info("Waiting for element visibility");
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.visibilityOf(element));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void waitForVisibleAndClick(WebDriver driver, WebElement element) {
		try {
			Thread.sleep(1000);
			Log.info("Waiting for element visibility");
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.visibilityOf(element)).click();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

//******************************************************************************************************************
	public static String screenShot(WebDriver driver, String filename) {
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot takesScreenshot = (TakesScreenshot) driver;
		File source = takesScreenshot.getScreenshotAs(OutputType.FILE);
		String destination = System.getProperty("user.dir") + "/ScreenShots/" + filename + "_" + dateName + ".png";
		// String destination = System.getProperty("user.dir") + "/GahCom/ScreenShots/"
		// + filename + "_" + dateName + ".png";

		Log.info("This is the Path of the Screenshhot : " + destination);

		try {
			FileUtils.copyFile(source, new File(destination));
		} catch (Exception e) {
			e.getMessage();
		}

		// This new path for jenkins
		String newImageString = "http://localhost:8082/job/MyStoreProject/ws/MyStoreProject/ScreenShots/" + filename
				+ "_" + dateName + ".png";
		return destination;

		// return newImageString;

	}

	public String getCurrentTime() {
		String currentDate = new SimpleDateFormat("yyyy-MM-dd-hhmmss").format(new Date());
		return currentDate;
	}
	// *******************************************************************************************************************

	public static void ConvertXlsxToCSV(String xlsl, String csv) throws IOException, InvalidFormatException {
		// Load the Excel file
		XSSFWorkbook input = new XSSFWorkbook(new File(xlsl));

		// Create a CSVPrinter and specify the output CSV file
		CSVPrinter output = new CSVPrinter(new FileWriter(csv), CSVFormat.DEFAULT);

		// Assume you want to read data from the first sheet
		XSSFSheet sheet = input.getSheetAt(0);

		// Iterate over the rows in the sheet
		for (Row row : sheet) {
			// Create a StringBuilder to store the CSV row
			StringBuilder csvRow = new StringBuilder();

			// Iterate over the cells in the row
			for (Cell cell : row) {
				if (csvRow.length() > 0) {
					csvRow.append(",");
				}
				csvRow.append(cell.toString());
			}

			// Print the CSV row
			output.printRecord(csvRow.toString());
		}

		// Close the CSVPrinter and the input XSSFWorkbook
		output.close();
		input.close();

		System.out.println("Excel file has been converted to CSV.");
	}
}
