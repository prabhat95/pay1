import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

public class Read_PDF {

	static Set<String> names = new LinkedHashSet<>();

	public static List<String> getListOfNames() throws IOException {

		File file = new File("/home/prabhat/eclipse-workspace/Pay1/DataProvider/IndianOracle.com-Baby-Names.pdf");

		String filePath = file.getAbsolutePath();

		PDDocument document = PDDocument.load(new File(filePath));

		PDFTextStripper pdfTextStripper = new PDFTextStripper();

		String pdfContent = pdfTextStripper.getText(document);

		String[] lines = pdfContent.split("\\r?\\n");

		for (String line : lines) {

			if (line.contains("=")) {

				String[] h2 = line.split("=");
				System.out.println(" >>" + h2[0]);

				names.add(h2[0]);

			}
		}
		int j = 1;
		System.out.println("Number of Names in list : " + names.size());
		for (String name : names) {

			System.out.println(j + "." + name);

			j++;
			if (j == 3) {
				break;
			}
		}
		List<String> uniqueNames = new ArrayList<>(names);
		System.out.println("21th name in the list : " + uniqueNames.get(20));

		return uniqueNames;
	}
}
