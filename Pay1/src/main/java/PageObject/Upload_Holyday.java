package PageObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import Action.Action;
import BaseClass.Baseclass;

public class Upload_Holyday extends Baseclass {

	public Upload_Holyday() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[@class ='col-12 text-end']//a")
	WebElement Downlaod_Sample_Holyday;

	@FindBy(xpath = "//input[@type='file']")
	WebElement Upload_Holyday_Excel;

	@FindBy(xpath = "//button[@type='submit']")
	WebElement Submit_Button;

	@FindBy(xpath = "//div[@class='col-sm-12 api-response-message']")
	WebElement Actual_Validation;

	public void Upload_Holydays() throws IOException, InterruptedException {

		Action.JSClick(driver, Downlaod_Sample_Holyday);
//			Downlaod_Sample_Holyday.click();

		Thread.sleep(1000);

		String filepath = "/home/prabhat/git/repository2/Pay1/Pay1/Downloaded_Pdf//upload_holiday (1).xlsx";

		FileInputStream fis = new FileInputStream(filepath);
		
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		Sheet sheet = workbook.getSheetAt(0);

		String[][] holydayData = {
		    { "1", "Diwali-10", "10/11/23" }, 
		    { "2", "Diwali-11", "11/11/23" },
		    { "3", "Diwali-12", "12/11/23" },
		    { "4", "Diwali-13", "13/11/23" }, 
		    { "5", "Diwali-14", "14/11/23" }
		};

		for (int j = 0; j < holydayData.length; j++) {
			
		    Row row = sheet.createRow(sheet.getLastRowNum() + 1); // Create a new row

		    for (int i = 0; i < 3; i++) {
		        Cell cell = row.createCell(i);
		        String holy = holydayData[j][i];
		        cell.setCellValue(holy);
		        System.out.print(holy);
				System.out.print(" ");
		    }
		}

		FileOutputStream out = new FileOutputStream(filepath);
		workbook.write(out);
		out.close();
		workbook.close();
			Upload_Holyday_Excel.sendKeys(filepath);

		Submit_Button.submit();

		Action.waitForVisible(driver, Actual_Validation);

		String Actual_Validation1 = Actual_Validation.getText();

		String Expected_Validation_On_Success = "Holiday Successfully uploaded";

		String Expected_Validation_On_Fail = "Holiday upload Failed !!";

		Assert.assertEquals(Actual_Validation1, Expected_Validation_On_Success);

		System.out.println("File uploaded successfully and Validation Displayed : " + Actual_Validation1);

	}
}
