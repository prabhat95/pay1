package PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import Action.Action;
import BaseClass.Baseclass;
import Utility.Extent;
import Utility.ListenerClass;
import Utility.Log;


public class Pay1Login extends Baseclass {

	ExtentReports extent;
	ExtentTest test;
	Extent manager;
	ListenerClass ls;
	
	// Initialization
	public Pay1Login() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//input[@placeholder='Enter user name']")
	 WebElement emailInput;

	@FindBy(xpath = "//input[@id='password']")
	 WebElement passwordInput;

	@FindBy(xpath = "//button[@type='submit']")
	 WebElement submitButton;

//Usage
	public void Enteremail() {
		
		emailInput.sendKeys("Test_merchant");
		
	}

	public void enterPassword() {
		passwordInput.sendKeys("Test@123");
	}

	public void clickSubmitButton() {
		submitButton.click();

	}

	public void Pay1_Login() {

		try {

			emailInput.sendKeys("Test_merchant");
			
			Log.info("User Name : Test_merchant");
			
			test=Extent.test.info("User Name : Test_merchant");
			
			Thread.sleep(1000);
			
			passwordInput.sendKeys("Test@123");
			Log.info("Password Entered : Test@123");
			test=Extent.test.info("Password Entered : Test@123");
			
			Thread.sleep(1000);
			
			submitButton.click();
			Log.info("Clicked On Submit button & Login Successfull");
			test=Extent.test.info("Clicked On Submit button & Login Successfull");
			
			Thread.sleep(800);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void Click_On_New_HR() {

		Action.waitForVisible(driver, driver.findElement(By.linkText("New HR")));

		driver.findElement(By.linkText("New HR")).click();

	}

	public void Click_On_Upload_Holydays() {

		Action.waitForVisible(driver, driver.findElement(By.linkText("Upload Holiday")));

		driver.findElement(By.linkText("Upload Holiday")).click();

	}
	
	public void Click_On_Fine_Configuration() {
		
		WebElement fineConfiguration = driver.findElement(By.linkText("Fine Configuration"));
		
		Action.waitForVisible(driver, fineConfiguration);
		
		fineConfiguration.click();
		
		
	}
	
	

}
