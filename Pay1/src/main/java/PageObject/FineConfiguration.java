package PageObject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import Action.Action;
import BaseClass.Baseclass;

public class FineConfiguration extends Baseclass {

	public FineConfiguration() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//span[@class='imformative-title']")
	WebElement title;

	@FindBy(id = "fineChargesday")
	WebElement fineChargesday;

	@FindBy(id = "fineDay")
	WebElement fineDay;

	@FindBy(xpath = "//input[@type='submit']")
	WebElement Submit_Button;

	@FindBy(id = "attendanceday")
	WebElement Attendance_Type;

	@FindBy(id = "message")
	WebElement Validation_MEssage;

	@FindBy(xpath = "//div[@class = 'col-md-4']")
	WebElement Number_Of_field;

	@FindBy(xpath = "//div[@class = 'col-md-4']//ul")
	WebElement Mandatory_validation;

	String ExpectedValidation = "Save Successfully!!";

	public void Fine_Configuration() throws InterruptedException {

		Action.waitForVisible(driver, title);
		String getHeader = title.getText();
		System.out.println("Header Of the Page >>" + getHeader);

		System.out.println("Verifying Validation displayed on Empty Fields.");
		fineChargesday.clear();
		Thread.sleep(800);
		fineDay.clear();
		Thread.sleep(800);
		
		Submit_Button.click();
		System.out.println("Clicked On Submit button.");
		
		Action.waitForVisible(driver, Mandatory_validation);
		
		String Actualvalidation = Mandatory_validation.getText();
		
		System.out.println("Validation on Empty Fields : " +Actualvalidation);
		
		Assert.assertEquals(Mandatory_validation.isDisplayed(), true);
		
		System.out.println("Validation Displayed When clicked Submit button with Empty field");
	
		Thread.sleep(500);
		
		fineChargesday.sendKeys("1");
		
		Thread.sleep(500);
		fineDay.sendKeys("2");

		String atte = Attendance_Type.getText();
		System.out.println("Fine Configuration is for attendance type : " + atte);

		Submit_Button.click();
		
		Action.waitForVisible(driver, Validation_MEssage);

		boolean ValidationDisaplyed = Validation_MEssage.isDisplayed();
		Assert.assertEquals(ValidationDisaplyed, true);

		String Actualvalidation1 =Validation_MEssage.getText();
		
		Assert.assertEquals(Actualvalidation1, ExpectedValidation);

		System.out.println("Settings Saved Successfully.");
		

	}

}
