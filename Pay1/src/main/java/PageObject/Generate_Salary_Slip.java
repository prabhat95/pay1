package PageObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import Action.Action;
import BaseClass.Baseclass;

public class Generate_Salary_Slip extends Baseclass {

	public Generate_Salary_Slip() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "year")
	WebElement Select_Year;
	@FindBy(id = "month")
	WebElement Select_Month;
	@FindBy(id = "generateSalaerySalarySlip")
	WebElement GenerateSalary_Button;
	@FindBy(tagName = "tr")
	List<WebElement> rows;
	@FindBy(xpath = "//button[@id ='downloadReport']")
	WebElement Download_Pdf_button;
//
//	@FindBy()
//	WebElement dd;
//	@FindBy()
//	WebElement dd;

	public void Donwload_Salary_Slip(String EmployeeName) throws IOException, InterruptedException {

		List<WebElement> cells = driver.findElements(By.xpath("//tr//td[3]"));

		int n = 0;

		for (WebElement cell : cells) {

			String cellvalue = cell.getText();

			String[] name = cellvalue.split("-");

			String empName = name[0];

			System.out.println(">> " + empName);

			n++;
			System.out.println("------ : " + n);

			if (empName.equalsIgnoreCase(EmployeeName)) {

				WebElement DonwloadBtn = driver.findElement(By.xpath("(//tr//td[4])[" + n + "]//button"));

				Action.waitForVisibleAndClick(driver, DonwloadBtn);

				Thread.sleep(1500);

				String cellvalue1 = cell.getText();

				System.out.println("cellvalue1 >> " + cellvalue1);

				Thread.sleep(500);

				// Read Salary sleep PDF and Verify Salary

			}
		}
	}

	public void verifySalaryDetails() throws IOException {

//		ArrayList<String> Salarydeatils = AddSalaryDetails.greadedeatils;

		int ctc = AddSalaryDetails.ctc;
		String[] SlabPercentage = Add_Grade.GradeValue;
		
		List<Integer> slabsPercentage = new ArrayList<Integer>();

		for (String slab : SlabPercentage) {
			int Slab = Integer.valueOf(slab);
			slabsPercentage.add(Slab);
		}

		int Basic = ctc * slabsPercentage.get(0) / 100;
		System.out.println(Basic);
		int Pf = ctc * slabsPercentage.get(1) / 100;
		System.out.println(Pf);
		int HRA = ctc * slabsPercentage.get(2) / 100;
		System.out.println(HRA);
		int TDS = ctc * slabsPercentage.get(3) / 100;
		System.out.println(TDS);
		int Conveyance = ctc * slabsPercentage.get(4) / 100;
		System.out.println(Conveyance);
		int Other = ctc * slabsPercentage.get(5) / 100;
		System.out.println(Other);
		int insurance = ctc * slabsPercentage.get(6) / 100;
		System.out.println(insurance);

		File file = new File("/home/prabhat/Downloads/abhi-6-October2023 (8).pdf");

//		String filePath = file.getAbsolutePath();

		PDDocument document = PDDocument.load(file);

		PDFTextStripper pdfTextStripper = new PDFTextStripper();

		String pdfContent = pdfTextStripper.getText(document);

		String[] lines = pdfContent.split("\\r?\\n");

//		int  i = 0;

		for (String line : lines) {

			if (line.contains("Salary Slip For The Month")) {
				System.out.println("Salary slip header : " + line);
			}

			int i = 1, j = 0;

			if (line.equalsIgnoreCase("Salary Details")) {

				for (String line1 : lines) {

					if (line.equalsIgnoreCase("Deductions")) {
						continue;
					}
					
					if (line.contains("Gross  Salary")) {
						System.out.println(" Gross salary & total Deduction : " + line);
					}
					
					if (line.contains("Net Payable")) {
						System.out.println("Net Salary : " + line);
						break;
					}
					
					String[] values = line1.split(" ");

					int value = Integer.valueOf(values[1]);
					
					switch (value) {
					
					case 1:
						System.out.println("Basic is as per Salary details :" +Basic);
						continue;
					case 2:
						System.out.println("PF is as per Salary Details : "+ Pf);
						continue;
					case 3:
						System.out.println("TDS is as per Salary Details : "+TDS);
						continue;
					case 4:
						System.out.println("HRA is as per Salary Details : "+HRA);
						continue;
					case 5:
						System.out.println("Other is as per Salary Details : "+Other);
						continue;
					case 6:
						System.out.println("Conveyance is as per Salary Details : "+Conveyance);
						continue;
					case 7:
						System.out.println("Insurance is as per Salary Details : "+insurance);
						continue;


					}

				}

			}

		}
	}
}
