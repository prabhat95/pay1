package PageObject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import BaseClass.Baseclass;

public class Assign_Asset_Page extends Baseclass{

	public Assign_Asset_Page() {
		// TODO Auto-generated constructor stub
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath ="//span[@class='imformative-title']")
	WebElement Header_Of_the_Page;
	@FindBy(id="employeeId")
	WebElement Employee_Dropdown;
	@FindBy(id="categoryid")
	WebElement product_Category_DropDown;
	@FindBy(id="productsubCategory")
	WebElement Product_subCategory_DropDown;
	@FindBy(id= "brandName")
	WebElement brandName_Dropdown;
	@FindBy(id="serialNumber")
	WebElement serialNumber_Dropdown;
	@FindBy(id= "taggingNumber")
	WebElement tagName_Input;
	@FindBy(id="qty")
	WebElement Qauntity_Dropdown;
	@FindBy(xpath= "//input[@value='save']")
	WebElement Save_Button;
	
	@FindBy(xpath= "//button[@class='btn']")
	WebElement Download_AssetSheet_Button;
	@FindBy(id= "file")
	WebElement Upload_File;
	@FindBy(xpath ="//button[@class='btn custom-button-hr px-5']")
	WebElement Upload_Btn;
	@FindBy(xpath="//div[@class='text-end font-weight-bold h6 api-response-message']")
	WebElement Validation_Message;
	
	
	
	
	
	
	
	
	
	
	

}
