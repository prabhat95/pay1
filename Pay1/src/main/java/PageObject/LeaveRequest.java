package PageObject;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import Action.Action;
import BaseClass.Baseclass;

public class LeaveRequest extends Baseclass{

//  Initialization
    public LeaveRequest() {
    PageFactory.initElements(driver,this);
    }
	
//	declaration
    
//  Click on the new HR
    @FindBy(xpath="//a[normalize-space()='New HR']")
    WebElement NewHR ;	

//    Click on the leave management
    @FindBy(xpath = "//a[normalize-space()='Leave Management']")
    WebElement ClickOnTheLeaveManagement;
    
//    Click on the leave Request
    @FindBy(xpath = "//a[@href='/getepayPortal/getLeaveRequested']")
    WebElement ClickOnTheLeaveRequest;
    
//    Click on the Date Field box
    @FindBy(xpath = "//input[@id='dateRange']")
    WebElement DateFieldBox;
    
//    Click on the previous select date field
    @FindBy(xpath = "/html/body/div[2]/div[2]/div[1]/table/tbody/tr[1]/td[6]")
    WebElement SelectDateField;
    
//  Click on the last select date field
  @FindBy(xpath = "/html/body/div[2]/div[2]/div[1]/table/tbody/tr[5]/td[7]")
  WebElement SelectDateField2;
    
//    Click on the calendar apply button
    @FindBy(xpath = "//button[normalize-space()='Apply']")
    WebElement ClickOnTheApplyButton;
    
//    Choose Filter Data
    @FindBy(xpath = "//select[@id='filterDataId']")
    WebElement ChooseFilerData;
    
//    Choose select employee
    @FindBy(xpath = "//option[@value='274']")
    WebElement Selectemployee;
    
//    Click on the filter button
    @FindBy(xpath = "//button[normalize-space()='Filter']")
    WebElement ClickOnTheFilterButton;

    
//    Click on the approve leave request button
    @FindBy(xpath = "//*[@id=\"datable_1\"]/tbody/tr[1]/td[7]/div/div[1]/form/button")
    WebElement ApproveButton;
    
//    Success message
    @FindBy(xpath = "/html/body/div[1]/div/div[2]/div[2]/div/div/div")
    WebElement SuccessMessagerun;
    
    
    
    
//    Usage
    
//  Usage
    
//        Click on the new HR
          public void clickNewHR() {
	      Action.TestLog("Clicked on NewHR Button.");
	      NewHR.click();
          }

//        Click on the leave management
          public void LeaveManagement() {
	      Action.TestLog("Clicked on Leave Management Button.");
	      ClickOnTheLeaveManagement.click();
          }

//        Click on the add leave
          public void LeaveRequestbutton1() {
          Action.TestLog("Clicked on Leave Request Button.");
          ClickOnTheLeaveRequest.click();
          }
    	
//          Date filed box
          public void DateFieldBox1() {
        	  Action.TestLog("Clicked on Leave Request Button.");
        	  DateFieldBox.click();
          }
          
          
//           Click on the selected date field
          public void selectedatefield1() {
        	  Action.TestLog("Clicked on select date field Button.");
        	  SelectDateField.click();
        	  
          }
          
//        Click on the selected date field
       public void selectedatefield2() {
     	  Action.TestLog("Clicked on select date field Button.");
     	 SelectDateField2.click();
       }
          
//           Click on the calendar apply button
          public void ClickOnTheApplyButton1() {
        	  Action.TestLog("Click on the calendar apply button");
        	  ClickOnTheApplyButton.click();
          }

//       Choose Filter Data
          public void FilterData() {
        	  Action.TestLog("Choose Filter Data");
        	  Select Sel = new Select (ChooseFilerData);
  			Sel.selectByIndex(1);
          }
         
//        Select employee
          public void SelectEmployee1(){
        	  Action.TestLog("Select employee");
        	  Selectemployee.click();
          }
          
//         Click on the filter button
          public void ClickOnTheFilterButton1() {
        	  Action.TestLog("Click on the filter button");
        	  ClickOnTheFilterButton.click();
          }
          
//          Click on the approve leave request button
          public void ClickOnTheApproveButton() {
        	  Action.TestLog(" Click on the approve leave request button");
        	  ApproveButton.click();
        	  Alert alert = driver.switchTo().alert();
        	  alert.accept();
        	  
          }
          
//        Success Message
          public void SuccessMessage() {
        	  String ActualToast = SuccessMessagerun.getText();
 		     System.out.println(SuccessMessagerun.getText());
 		     String Expectedvalidation = "Status changed successfully !!";
 		     Assert.assertEquals(ActualToast, Expectedvalidation);
 		}
}
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
       