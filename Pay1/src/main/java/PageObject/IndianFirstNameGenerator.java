package PageObject;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import Action.Action;
import BaseClass.Baseclass;

public class IndianFirstNameGenerator extends Baseclass {

	ArrayList names = new ArrayList<>();

	List<WebElement> Names = driver.findElements(By.xpath("https://www.momjunction.com/baby-names/indian/"));
	WebElement Next = driver.findElement(By.xpath("//a[@class='next page-numbers']"));

	
	public void collect_names() throws InterruptedException {

		Thread.sleep(500);
		driver.get("https://www.momjunction.com/baby-names/indian/");

		Thread.sleep(1000);

		for (int i = 0; i < 10; i++) {

			for (WebElement n : Names) {

				String name = n.getText();
				names.add(name);

			}

			Action.JSClick(driver, Next);
		}
	}
}
