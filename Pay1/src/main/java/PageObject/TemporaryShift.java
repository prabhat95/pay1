package PageObject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import Action.Action;
import BaseClass.Baseclass;


public class TemporaryShift extends Baseclass {

	//  Initialization
    public TemporaryShift() {
    PageFactory.initElements(driver,this);
    }
	
//  declaration
//  Enter email
  @FindBy(xpath = "//input[@placeholder='Enter user name']")
  WebElement Login;
//  Enter Password
  @FindBy(xpath = "//input[@id='password']")
  WebElement Password;
//  CLick on the login button
  @FindBy(xpath = "//button[@type='submit']")
  WebElement LoginClick;
//  Click on the new HR
  @FindBy(xpath="//a[normalize-space()='New HR']")
  WebElement NewHR ;	
//  Click on the attendance
  @FindBy(xpath="//a[normalize-space()='Attendance']")
	WebElement Attendence;
//  Click on the Temporary Shift
   @FindBy(xpath="//a[contains(text(),'Temporary')]")
   WebElement TemporaryShift;
//   Choose Employee Name
  @FindBy(xpath = "//select[@id='selectEmp']")
  WebElement EmployeeName;
//  Choose working shift
  @FindBy(xpath = "//select[@id='shiftName']")
  WebElement WorkingShift;
//  Enter select date
  @FindBy(xpath = "//input[@id='workingDate']")
    WebElement SelectDate;
//    Click on the save button
  @FindBy(xpath = "//button[@id='formSubmit']")
  WebElement SaveButtons;
//    Successful 
   @FindBy(xpath = "/html/body/div/div/div[2]/div[2]/div[1]/div")
  WebElement SuccessfulMessege;
    
    
//  Usage 

//Enter email 
  public void Login() {
  	Action.TestLog("Enter Valid email Id or username");
  	Login.sendKeys("Test_merchant");
  }
//  Enter password
  public void Password() {
  	Action.TestLog("Enter Valid password");
  	Password.sendKeys("Test@123");
  }
//  Click on the login button
  public void ClickLoginButton() {
  	Action.TestLog("Clicked on Login Button.");
  	LoginClick.click();
  }
//	   Click on the new HR
  public void clickNewHR() {
  	Action.TestLog("Clicked on NewHR Button.");
  	NewHR.click();
  }
  
//  Click on the attendance
  public void clickAttendenceBtn() {
  	Action.TestLog("Clicked on Attendence Button.");
  	Attendence.click();
  }
//  Click on the Temporary shift
  public void ClickOnTheTemporaryShift() {
	  	Action.TestLog("Choose on Temporary Shift Box.");
	  	TemporaryShift.click();
  }
//  Choose employee name
	  	 public void EmployeeName() {
	 	  	Action.TestLog("Choose on Employee Name Box.");
	 	  	Select Sel = new Select (EmployeeName);
			Sel.selectByIndex(3);
	  	 }
//	  Enter Working shift
	  	 public void WorkingShift() {
		 	  	Action.TestLog("Enter on Working Shift Button.");
		 	  	Select Sel = new Select (WorkingShift);
				Sel.selectByIndex(18);
	  	 }
//	  Enter select date
		 	   public void SelectDate() {
			 	  	Action.TestLog("Enter on Select Date Button.");
			 	  	SelectDate.sendKeys("18-10-2023");
		 	   }
//  Click on the save button
			 	   public void SaveButtons() {
				 	  	Action.TestLog("Click on the save button.");
				 	  	SaveButtons.click();  	
  
	}
//   Successful message
		public void SuccessfulMessege() {
			 String ActualToast = SuccessfulMessege.getText();
		     System.out.println(SuccessfulMessege.getText());
		     String Expectedvalidation = "Attendance Config saved successfully !!";
		     Assert.assertEquals(ActualToast, Expectedvalidation);
		}
		}

