package PageObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.github.javafaker.Faker;

import Action.Action;
import BaseClass.Baseclass;
import Extra.Read_PDF;

public class AddEmployees extends Baseclass {

	String user = "Test_merchant";
	String pass = "Test@123";
	
	Read_PDF n = new Read_PDF();
	
	public AddEmployees() {
		// TODO Auto-generated constructor stub
		PageFactory.initElements(driver, this);
	}

	@FindBy(linkText = "New HR")
	WebElement NewHR;

	@FindBy(id = "dropdownMenuButton1")
	WebElement Employee;

	@FindBy(linkText = "Add Employee")
	WebElement addEmployee;

	@FindBy(id = "empId")
	WebElement employeeID;

	@FindBy(id = "empName")
	WebElement empName;

	@FindBy(id = "uName")
	WebElement username;

	@FindBy(id = "mobileNo")
	WebElement MobileNumber;

	@FindBy(id = "createDate")
	WebElement joiningDate;

	@FindBy(id = "dateOfBirth")
	WebElement DOB;

	@FindBy(id = "department")
	WebElement department;

	@FindBy(id = "designation")
	WebElement designation;

	@FindBy(id = "email")
	WebElement email;

	@FindBy(id = "reportingPselect")
	WebElement reportingPersone;

	@FindBy(id = "userType")
	WebElement userType;

	@FindBy(id = "select2-reportingPselect-container")
	WebElement SelectGrade;

	@FindBy(xpath = "//input[@type='search']")
	WebElement Search;

	@FindBy(id = "address")
	WebElement address;

	@FindBy(id = "city")
	WebElement city;

	@FindBy(id = "country")
	WebElement country;

	@FindBy(id = "state")
	WebElement state;

	@FindBy(id = "pincode")
	WebElement pincode;

	@FindBy(id = "geoTracking")
	WebElement Tracking;

	@FindBy(xpath = "//input[@type='submit']")
	WebElement SubmitButton;

	@FindBy(xpath = "//input[@name='username']")
	WebElement EnterUsermane;

	@FindBy(xpath = "//input[@name='password']")
	WebElement EnterPassword;

	@FindBy(xpath = "//button[@type='submit']")
	WebElement LoginButton;

	static Faker faker = new Faker();
	static Random random = new Random();
	int minValue = 1;
	int maxValue = 100;
	int randomNumber = random.nextInt(maxValue - minValue + 1) + minValue;

	public void newHR(int n, int m) throws InterruptedException, IOException {
		
		String path = "/home/prabhat/git/repository/GetePay/DataProvider/EmployeeSheet.xlsx";

		FileInputStream fis = new FileInputStream(path);
		Workbook workbook = new XSSFWorkbook(fis);
		Sheet sheet = workbook.getSheetAt(0);
		FileOutputStream fos = null;

		Thread.sleep(800);

		EnterUsermane.sendKeys(user);
		Thread.sleep(800);

		EnterPassword.sendKeys(pass);
		Thread.sleep(800);

		Action.JSClick(driver, LoginButton);
		Thread.sleep(1500);

//		WebDriverWait wait = new WebDriverWait(driver, 20);
		Action.waitForVisible(driver, NewHR);
		Action.JSClick(driver, NewHR);

			Thread.sleep(1500);
			Row row = sheet.createRow(sheet.getLastRowNum() + 1);

			Thread.sleep(700);
//			int ID = random.nextInt(700, 1000);
			String EmpID = "GET" + randomNumber;

			employeeID.clear();
			employeeID.sendKeys(EmpID);
			System.out.println("Employee ID : " + EmpID);

			Cell cell0 = row.createCell(0);
			cell0.setCellValue(EmpID);

			Thread.sleep(700);
//			String emplyName = getListOfNames().get(i);
			empName.clear();
			String firstName = faker.name().firstName();
			empName.sendKeys(firstName);	
		
			System.out.println(firstName);

			Cell cell1 = row.createCell(1);
			cell1.setCellValue(firstName);

			Thread.sleep(700);
			MobileNumber.clear();
			String ContactNumber = faker.phoneNumber().subscriberNumber(10);
			MobileNumber.sendKeys("0123456789");

			Thread.sleep(700);
			joiningDate.clear();
			String date = RandomDate();
			joiningDate.sendKeys(date);
			System.out.println("join date : " + date);

			Thread.sleep(700);
			DOB.clear();
			DOB.sendKeys("15/02/1999");

			department.clear();
			department.sendKeys("Technical");

			Thread.sleep(700);
			designation.clear();
			String desg = faker.job().position();
			designation.sendKeys(desg);
			System.out.println("designation : " + desg);

			Thread.sleep(700);
			email.clear();
			String Email = firstName + randomNumber + "@test.com"; // email
			email.sendKeys(Email);
			System.out.println(Email);

			Thread.sleep(700);

			reportingPersone.sendKeys("Aniru");
			reportingPersone.sendKeys(Keys.ENTER);

			userType.sendKeys("Office");
			userType.sendKeys(Keys.ENTER);

			Thread.sleep(700);

			SelectGrade.click();
			Search.click();
			Search.sendKeys("TStark");
			Search.sendKeys(Keys.ENTER);

			Thread.sleep(700);
			address.clear();
			String add = faker.address().fullAddress();
			address.sendKeys(add);

			city.clear();
			String cty = faker.address().city();
			city.sendKeys(cty);

			Thread.sleep(700);
			country.clear();
			String contry = faker.address().country();
			country.sendKeys(contry);

			Thread.sleep(700);
			pincode.clear();
			String stt = faker.address().state();
			state.sendKeys(stt);
			pincode.sendKeys("302001");
			
			boolean yesits = pincode.isDisplayed();
			Assert.assertEquals(yesits, true);

			Thread.sleep(1000);
			Action.JSClick(driver, Tracking);
			Tracking.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);

			Thread.sleep(700);
			SubmitButton.click();

			fos = new FileOutputStream(path);

			workbook.write(fos);
			Thread.sleep(1000);

//			driver.switchTo().alert().accept();

			Thread.sleep(1000);
			Employee.click();

			Thread.sleep(800);
			addEmployee.click();

		}


	

	public static String RandomDate() {
		// Create an instance of the Random class
		Random random = new Random();

		// Define a date range (start and end dates)
		Calendar startDate = new GregorianCalendar(2023, Calendar.JULY, 1);
		Calendar endDate = new GregorianCalendar(2023, Calendar.SEPTEMBER, 30);

		// Calculate the difference in milliseconds between the start and end dates
		long startMillis = startDate.getTimeInMillis();
		long endMillis = endDate.getTimeInMillis();
		long randomMillis = startMillis + (long) (random.nextDouble() * (endMillis - startMillis));

		// Create a Calendar instance with the random date
		Calendar randomDate = new GregorianCalendar();
		randomDate.setTimeInMillis(randomMillis);

		// Extract year, month, and day from the random date
		int year = randomDate.get(Calendar.YEAR);
		int month = randomDate.get(Calendar.MONTH) + 1; // Month is 0-based, so add 1
		int day = randomDate.get(Calendar.DAY_OF_MONTH);

		String inty = String.valueOf(year);
		String intm = String.valueOf(month);
		String intd = String.valueOf(day);

		String date;

		if (intm.equalsIgnoreCase("11") || intm.equalsIgnoreCase("10") || intm.equalsIgnoreCase("12")) {
			date = intd + "/" + intm + "/" + inty;
			System.out.println("return Date : " + date);

		} else {
			date = intd + "/" + "0" + intm + "/" + inty;
			System.out.println("return Date : " + date);

		}

		return date;

	}

	
	
	static Set<String> name1 = new LinkedHashSet<>();

	
	public static List<String> getListOfNames() throws IOException {

		File file = new File("/home/prabhat/eclipse-workspace/Pay1/DataProvider/IndianOracle.com-Baby-Names.pdf");

		String filePath = file.getAbsolutePath();

		PDDocument document = PDDocument.load(new File(filePath));

		PDFTextStripper pdfTextStripper = new PDFTextStripper();

		String pdfContent = pdfTextStripper.getText(document);

		String[] lines = pdfContent.split("\\r?\\n");

		for (String line : lines) {

			if (line.contains("=")) {

				String[] h2 = line.split("=");
				System.out.println(" >>" + h2[0]);

				name1.add(h2[0]);

			}
		}
		int j = 1;
		System.out.println("Number of Names in list : " + name1.size());
		for (String name : name1) {

			System.out.println(j + "." + name);

			j++;
			if (j == 3) {
				break;
			}
		}
		List<String> uniqueNames = new ArrayList<>(name1);
		System.out.println("21th name in the list : " + uniqueNames.get(20));

		return uniqueNames;
	}
	
	
}
