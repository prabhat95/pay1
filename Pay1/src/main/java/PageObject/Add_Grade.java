package PageObject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import Action.Action;
import BaseClass.Baseclass;

public class Add_Grade extends Baseclass {

	public static String[] GradeValue = { "40", "20", "12", "20", "5", "5", "10" };

	public Add_Grade() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "(//ul[@class='navbar-nav-items']//li[3])[2]")
	WebElement Assign_Grade;
	@FindBy(id = "gradeName")
	WebElement select_grade;
	@FindBy(xpath = "//div[@id='addGrade']//button")
	WebElement AddGradeButton;
	@FindBy(id = "gradeNameDetail")
	WebElement EnterGradeName;
	@FindBy(id = "slab_2")
	WebElement EneterSlab;
	@FindBy(id = "subBtn")
	WebElement SubmitButton;
	@FindBy(id = "leaveInterval")
	WebElement leaveInterval;
	@FindBy(id = "PF")
	WebElement EnterPF;
	@FindBy(id = "gradeFieldList2.slabType")
	WebElement Deductable;
	@FindBy(xpath = "(//div[@class='col']//i)[3]")
	WebElement deleteExtraLeave;
	@FindBy(id = "message")
	WebElement validationMessage;
	@FindBy(xpath = "//div[@class='col-sm-2']//button[@type='submit']")
	WebElement Delete_Grade;

	String val = "Salary Grade Created Successfully! !!";
	String ExstingGradeValidation = "This grade name already present.Please provide another grade name !!";
	String deletingGradeValidation = "Salary grade deleted successfully !!";

	public void Create_New_Grade(String GradeName) throws InterruptedException {

		Assign_Grade.click();

		Thread.sleep(1000);

		Select grade = new Select(select_grade);

		grade.selectByValue("other");

		Thread.sleep(800);

		AddGradeButton.click();
		Thread.sleep(500);

		EnterGradeName.sendKeys(GradeName);

		EnterPF.sendKeys("20");

		for (int i = 0; i < 7; i++) {

			WebElement slab = driver.findElement(By.xpath("//input[@id='slab_" + i + "']"));

			Action.type(slab, GradeValue[i]);

			WebElement labels = driver.findElement(By.xpath("//input[@id='gradeFieldList" + i + ".labelName']"));
			String labelName = labels.getAttribute("value");

			System.out.println("label Name : " + labelName);

			System.out.println(labelName + " : " + GradeValue[i]);

			if (labelName.equalsIgnoreCase("pf")) {
				WebElement slabeType = driver.findElement(By.xpath("//select[@id='gradeFieldList" + i + ".slabType']"));
				slabeType.click();
				slabeType.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);

			} else if (labelName.equalsIgnoreCase("tds")) {
				WebElement slabeType = driver.findElement(By.xpath("//select[@id='gradeFieldList" + i + ".slabType']"));
				slabeType.click();
				slabeType.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);

			} else if (labelName.equalsIgnoreCase("Insurance")) {
				WebElement slabeType = driver.findElement(By.xpath("//select[@id='gradeFieldList" + i + ".slabType']"));
				slabeType.click();
				slabeType.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);

			}

		}

		String leaves_For_Interval = leaveInterval.getText();
		System.out.println("leaves_For_Interval : " + leaves_For_Interval);

		String[][] x = { { "24", "6", "9", "10" }, { "12", "2", "3", "5" } };

		// Output each array element's value
		WebElement Total_Paid_Leave = driver.findElement(By.xpath("//input[@id = 'totalLeave_0']"));
		Total_Paid_Leave.sendKeys(x[0][0]);

		WebElement Total_Sick_Leave = driver.findElement(By.xpath("//input[@id = 'totalLeave_1']"));
		Total_Sick_Leave.sendKeys(x[1][0]);

		WebElement Monthly_Paid_Leave = driver.findElement(By.xpath("//input[@id = 'intervalLeaveDays_0']"));
		Monthly_Paid_Leave.sendKeys(x[0][1]);

		WebElement Monthly_Sick_Leave = driver.findElement(By.xpath("//input[@id = 'intervalLeaveDays_1']"));
		Monthly_Sick_Leave.sendKeys(x[1][1]);

		WebElement Contineous_Paid_Leave = driver.findElement(By.xpath("//input[@id = 'continuousLeave_0']"));
		Contineous_Paid_Leave.sendKeys(x[0][2]);

		WebElement Contineous_Sick_Leave = driver.findElement(By.xpath("//input[@id = 'continuousLeave_1']"));
		Contineous_Sick_Leave.sendKeys(x[1][2]);

		WebElement Carryforward_Paid_Leave = driver.findElement(By.xpath("//input[@id = 'caryForwardLeave_0']"));
		Carryforward_Paid_Leave.sendKeys(x[0][3]);

		WebElement Carryforward_Sick_Leave = driver.findElement(By.xpath("//input[@id = 'caryForwardLeave_1']"));
		Carryforward_Sick_Leave.sendKeys(x[1][3]);

		Thread.sleep(500);

		deleteExtraLeave.click();
		Thread.sleep(500);

		SubmitButton.click();

		Action.waitForVisible(driver, validationMessage);

		boolean ValidationDisplayed = validationMessage.isDisplayed();
		Assert.assertEquals(ValidationDisplayed, true);

		String validation = validationMessage.getText();
		System.out.println("Validation Displayed : " + validation);

	}

	public void Delete_Grade(String GradeName) throws InterruptedException {

		Select grade = new Select(select_grade);

		grade.selectByValue(GradeName);
		Thread.sleep(500);

		Delete_Grade.click();
		
		String text = driver.switchTo().alert().getText();
		System.out.println("Text On Alert : "+ text);
		driver.switchTo().alert().accept();

		Action.waitForVisibleAndGetText(driver, validationMessage);

		boolean ValidationDisplayed = validationMessage.isDisplayed();
		
		Assert.assertEquals(ValidationDisplayed, true);

		// Verifying that the grade is not present in the list...
		
		List<WebElement> options = grade.getOptions();

		boolean isValuePresent = false;

		for (WebElement option : options) {
			if (option.getText().equals(GradeName)) {
				isValuePresent = true;
				break;
			}
		}

		// Now, 'isValuePresent' will be true if the desired value is present, otherwise
		// false
		if (isValuePresent) {
			System.out.println(" Grade is not deleted yet..");
		} else {
			System.out.println(" Grade Deleted Successfully...");
		}

	}

}
