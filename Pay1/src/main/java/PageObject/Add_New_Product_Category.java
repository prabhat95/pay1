package PageObject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import BaseClass.Baseclass;

public class Add_New_Product_Category extends Baseclass {

	public Add_New_Product_Category() {
		
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="categoryName")
	WebElement categoryName_Inputbox;
	@FindBy(id= "description")
	WebElement description_Inputbox;
	@FindBy(id="addProductCatSaveBtn")
	WebElement addProductCatSaveBtn;
	@FindBy(id="category")
	WebElement category_Dropdown;
	@FindBy(id="subCategoryName")
	WebElement subCategoryName_Inputbox;
	@FindBy(id="prodctSavebtn")
	WebElement ProductSavebutton;
	
	
	
}
