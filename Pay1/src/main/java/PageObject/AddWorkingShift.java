package PageObject;

import javax.swing.JOptionPane;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import BaseClass.Baseclass;


public class AddWorkingShift extends Baseclass {

	
//  Initialization
    public AddWorkingShift() {
    PageFactory.initElements(driver,this);
    }

	
//      declaration
	     
//	    Enter email
	    @FindBy(xpath = "//input[@placeholder='Enter user name']")
	    WebElement Login;
//	    Enter Password
	    @FindBy(xpath = "//input[@id='password']")
	    WebElement Password;
//	    CLick on the login button
	    @FindBy(xpath = "//button[@type='submit']")
	    WebElement LoginClick;
//	    Click on the new HR
	    @FindBy(xpath="//a[normalize-space()='New HR']")
	    WebElement NewHR ;	
//	    Click on the attendance
	    @FindBy(xpath="//a[normalize-space()='Attendance']")
		WebElement Attendence;
//      Click on the working shift
		@FindBy(xpath="//a[contains(text(),'Working')]")
		WebElement WorkingShift;
//		Enter Shift name
		@FindBy(xpath="//input[@id='shiftName']")
		WebElement ShiftName;
//		Enter Shift Time
		@FindBy(xpath="//input[@id='shiftTime']")
		WebElement ShiftTime;
//		Choose ISDefault
		@FindBy(xpath="//select[@id='isDefault']")
		WebElement IsDefault;
//		Click on the save button
		@FindBy(xpath="//input[@value='Save']")
		WebElement SaveButton;
//		Catch success Message
		@FindBy(xpath = "//div[@class='col-sm-8 api-response-message']//div/span")
		WebElement SuccessMessage;
//      Click and enter name search box
		@FindBy(xpath="//input[@type='search']")
		WebElement SearchNameButton;
//		Click on the edit button
		@FindBy(xpath = "//*[@id=\"datable_1\"]/tbody/tr/td[4]/div/div/form/button")
		WebElement EditButton;
//		Edit default setting
		@FindBy(xpath = "//select[@id='isDefault']")
		WebElement editToTheSelect;
//		Click on the again save button
		@FindBy(xpath = "//input[@value='Save']")
		WebElement ClickOnTheagainSaveButton;
//		Catch success message
		@FindBy(xpath = "//*[@id=\"attendanceShift\"]/div/div/div/span")
		WebElement EditCatchsuccessmessage;
//		Again search shiftName
		@FindBy(xpath = "//input[@type='search']")
		WebElement AgainSearchShiftName;
//		Click on the Again edit button
		@FindBy(xpath = "//*[@id=\"datable_1\"]/tbody/tr/td[4]/div/div/form/button")
		WebElement AgainClickOnTheEditButton;
//		Again edit default setting
		@FindBy(xpath = "//select[@id='isDefault']")
		WebElement EditToTheDefault;
//		Again Click On The Save Button
		@FindBy(xpath = "//input[@value='Save']")
		WebElement AgainClickOnTheSaveBtn;
//		Catch Default success Message
		@FindBy(xpath = "//*[@id=\"attendanceShift\"]/div/div")
		WebElement CatchDefaultSuccessMessage;
	
   
    
        
//      usage
//      Enter email 
        public void Login() {
        	Login.sendKeys("Test_merchant");
        }
//        Enter password
        public void Password() {
        	Password.sendKeys("Test@123");
        }
//        Click on the login button
        public void ClicLoginButton() {
        	LoginClick.click();
        }
// 	   Click on the new HR
        public void clickNewHR() {
        NewHR.click();
        }
        
//	    Click on the attendance
        public void clickAttendenceBtn() {
	    Attendence.click();
        }
//      Click on the working shift
	    public void clickWorkingShift() {
		WorkingShift.click();
	    }
//		Enter Shift name
		public void EnterShiftName () {
		String str = JOptionPane.showInputDialog("Enter shift name");
		ShiftName.sendKeys(str);
		}
//	    Enter shift Time
		public void EnterShiftTime () {
		ShiftTime.clear();
		ShiftTime.sendKeys("23:00:00 - 06:00:00");
		}
//		Choose ISDefault
		public void ChooseIsDefault () {
		Select Sel = new Select (IsDefault);
		Sel.selectByIndex(0);
		}
//	    Click on the save button
		public void ClickSaveButton () {
			SaveButton.click();
			}
//		Catch success Message
		public void CatchSuccessMessage() {
			 String ActualToast = SuccessMessage.getText();
		     System.out.println(SuccessMessage.getText());
		     String Expectedvalidation = "Working shift saved successfully !!";
		     Assert.assertEquals(ActualToast, Expectedvalidation);
		}
//	    Click and enter name search box
	    public void SearchNameButton() {
	    	SearchNameButton.sendKeys("default");
	    }
//	    Click on the edit button
	    public void EditButton() {
	    	EditButton.click();
	    }
//	    Edit default to select
	    public void editToTheSelect() {
	    	Select Sel = new Select (IsDefault);
			Sel.selectByIndex(0);
	    }
//	    Click on the save button
		public void ClickOnTheagainSaveButton () {
			ClickOnTheagainSaveButton.click();
		}
//	    Edit catch success message
		public void EditCatchsuccessmessage() {
			String ActualToast = EditCatchsuccessmessage.getText();
		     System.out.println(EditCatchsuccessmessage.getText());
		    // String Expectedvalidation = "Working shift updated successfully !! ";
		     Assert.assertEquals(ActualToast, "Working shift updated successfully !!");
		}
//	    Again search shift Name
		public void AgainSearchShiftName() {
			String str = JOptionPane.showInputDialog("Enter shift name");
			AgainSearchShiftName.sendKeys(str);
		}
//	    Again Click on the edit button
		public void AgainClickOnTheEditButton() {
			AgainClickOnTheEditButton.click();
		}
//	    Again edit select to default
		public void EditToTheDefault() {
			Select Sel = new Select (EditToTheDefault);
			Sel.selectByIndex(1);
		}
//		Click on the save button
		public void AgainClickOnTheSaveBtn() {
			AgainClickOnTheSaveBtn.click();
		}
//	    Catch Default success Message
		public void CatchDefaultSuccessMessage() {
			String ActualToast = CatchDefaultSuccessMessage.getText();
		     System.out.println(CatchDefaultSuccessMessage.getText());
		     String Expectedvalidation = "Working shift updated successfully !!";
		     Assert.assertEquals(ActualToast, Expectedvalidation);
		}
	    
	    
	    
	    }

