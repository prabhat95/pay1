package PageObject;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import Action.Action;
import BaseClass.Baseclass;

public class AddSalaryDetails extends Baseclass {

//	Initialization

	public AddSalaryDetails() {
		PageFactory.initElements(driver, this);
	}

	public static ArrayList<String> greadedeatils = new ArrayList<String>();

	String expectedMonth = "SEP 2023";
	String dateOne = "1";
	public static int ctc = 20000;
	String EmployeeName = "Anirudh";

//	Usage

//       Click on the new HR
	@FindBy(xpath = "//a[normalize-space()='New HR']")
	WebElement NewHR;

//	    Click add salary details
	@FindBy(xpath = "//a[@href='/getepayPortal/mweb/loadServices?m=salaryslip']")
	WebElement clickaddsalarydetails;

//	     Enter date in the date field
	@FindBy(xpath = "//input[@id='salaryDate']")
	WebElement Enterdate;

//	     Click on the select employee

	@FindBy(xpath = "//span[@role='combobox']")
	WebElement SelectEmployee;

//	     Search employee
	@FindBy(xpath = "//input[@aria-label='Search']")
	WebElement Searchemployee;

//     	Select grade
	@FindBy(xpath = "//select[@id='GradeData']")
	WebElement Selectgrade;

//     	Enter CTC
	@FindBy(xpath = "//input[@id='ctc']")
	WebElement EnterCTC;

//    Save Button
	@FindBy(xpath = "//input[@id='saveBtn']")
	WebElement SaveButton;

//      Success message
	@FindBy(xpath = "//*[@id=\"msgShow\"]")
	WebElement successmessage;

//	      Declaration

//        Click on the new HR
	public void clickNewHR1() {
		Action.TestLog("Clicked on NewHR Button.");
		NewHR.click();
	}

//	    Click on the add salary details

	public void addsalerydetail1() {
		Action.TestLog("Click on the Add Salary details");
		clickaddsalarydetails.click();
	}

//         Enter date in the date field
	public void calender() {
		Action.TestLog("CLick on the date field");
		Enterdate.sendKeys("01-10-2023");
	}

//         Click on the select employee
	public void selectemployee() {
		Action.TestLog("Click on the select employee");
		SelectEmployee.click();
	}

//        	  Search employee

	public void searchemployee() {
		Action.TestLog("Search employee");
		Searchemployee.sendKeys(EmployeeName);
		Searchemployee.sendKeys(Keys.ENTER);
	}

//        	  Select grade

	public void selectgrade(String gradeName) {

		Select select = new Select(Selectgrade);
		
		select.selectByValue(gradeName);

	}

//        	  Enter CTC

	public void enterCTC() {
		Action.TestLog("Enter CTC");
		EnterCTC.clear();
		
		String CTC = String.valueOf(ctc);
		EnterCTC.sendKeys(CTC);
	}

//        	  Check percentage
	public void Checkpercentage1() {

		
		for (int i = 1; i < 8; i++)

		{
			WebElement label = driver.findElement(By.xpath("(//div[@id='inputsDetails']//div//label)[" + i + "]"));
			WebElement CheckPercentage = driver
					.findElement(By.xpath("(//div[@id='inputsDetails']//div//input)[" + i + "]"));

			String value1 = label.getText();
			String value = CheckPercentage.getAttribute("value");

			System.out.println(value1 + " : " + value);

			
			greadedeatils.add(value);

		}
		double basic = Double.valueOf(greadedeatils.get(0));
		System.out.println("This is a basic " + basic);
		double pf = Double.valueOf(greadedeatils.get(1));
		double HRA = Double.valueOf(greadedeatils.get(2));
		double TDS = Double.valueOf(greadedeatils.get(3));
		double Conveyance = Double.valueOf(greadedeatils.get(4));
		double Other = Double.valueOf(greadedeatils.get(5));
		double Insurance = Double.valueOf(greadedeatils.get(6));
		
		//
	    String [] GradePercentageValue = Add_Grade.GradeValue;
	    
	    
	    String basicPercentage =  GradePercentageValue[0];
		String PfPercentage =  GradePercentageValue[1];
		String HRAPercentage =  GradePercentageValue[2];
		String TDSPercentage =  GradePercentageValue[3];
		String Conveyancepercentage =  GradePercentageValue[4];
		String OtherPercentage =  GradePercentageValue[5];
		String InsuarancePercentage =  GradePercentageValue[6];

		double Basic_percentage = (int) (basic / ctc) * 100;
		System.out.println("Basic Percentage: " + Basic_percentage);
		
		Assert.assertEquals(basicPercentage, Basic_percentage);
		

		double PF_percentage = (int) (pf / ctc * 100);
		System.out.println("PF_Percentage: " + PF_percentage);
		
		Assert.assertEquals(PfPercentage, PF_percentage);

		double HRA_percentage = (int) (HRA / ctc * 100);
		System.out.println("HRA_Percentage : " + HRA_percentage);
		
		Assert.assertEquals(HRAPercentage, HRA_percentage);

		double TDS_percentage = (int) (TDS / ctc * 100);
		System.out.println("TDS_Percentage : " + TDS_percentage);
		
		Assert.assertEquals(TDSPercentage, TDS_percentage);

		double Conveyance_percentage = (int) (Conveyance / ctc * 100);
		System.out.println("Conveyance_Percentage : " + Conveyance_percentage);
		
		Assert.assertEquals(Conveyancepercentage, Conveyance_percentage);

		double Other_percentage = (int) (Other / ctc * 100);
		System.out.println("Other_percentage : " + Other_percentage);
		
		Assert.assertEquals(OtherPercentage, Other_percentage);

		double Insurance_percentage = (int) (Insurance / ctc * 100);
		System.out.println("Insurance_Percentage : " + Insurance_percentage);
		
		Assert.assertEquals(InsuarancePercentage, Insurance_percentage);

	
	}

	public void savebutton() {

		SaveButton.click();
		Action.waitForVisible(driver, successmessage);

	}

//        	  success message

	public void successmessage1() {
		String ActualToast = successmessage.getText();
		System.out.println(successmessage.getText());
		String Expectedvalidation = "Salary Update Successfully !!";
		Assert.assertEquals(ActualToast, Expectedvalidation);
	}

}
