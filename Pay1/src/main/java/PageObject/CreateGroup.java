package PageObject;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Action.Action;
import BaseClass.Baseclass;



public class CreateGroup extends Baseclass{
    
	 @Test
     public void CreateNewGroup() throws InterruptedException, IOException {
		 
		 Thread.sleep(800);
		 driver.findElement(By.linkText("create Group")).click();
		 
//    enter email 
		 driver.findElement(By.xpath("//input[@placeholder='Enter user name']")).sendKeys("Test_merchant");
		 Action.screencapture();
		 Action.TestLog("Enter Valid email Id or username");
//		 Enter password
		 driver.findElement(By.xpath("//input[@id='password']")).sendKeys("Test@123");
		 Action.screencapture();
		 Action.TestLog("Enter Valid password username");
//		 CLick on the save button
		 driver.findElement(By.xpath("//button[@type='submit']")).click();
		 Action.screencapture();
		 Action.TestLog("Enter Submit button");
     
//   Click on the New HR
     driver.findElement(By.xpath("//a[normalize-space()='New HR']")).click();
     Thread.sleep(3000);
     Action.screencapture();
	 Action.TestLog("Click on the new hr");
//   Click on the Attendance
     driver.findElement(By.xpath("//a[normalize-space()='Attendance']")).click();
     Thread.sleep(3000);
     Action.screencapture();
	 Action.TestLog("Click on the Attendance");
//   Click on the create group
     driver.findElement(By.xpath("//a[contains(text(),'create')]")).click();
     Thread.sleep(3000);
     Action.screencapture();
	 Action.TestLog("Click on the create group");
//   Enter Group name 
     driver.findElement(By.xpath("//input[@id='groupName']")).sendKeys("Auto Testing");
     Thread.sleep(3000);
     Action.screencapture();
	 Action.TestLog("Enter Group name ");
//   Click on the save button
     driver.findElement(By.xpath("//input[@value='Save']")).click();
     Thread.sleep(3000);
     Action.screencapture();
	 Action.TestLog("Click on the save button");
//   Enter Group name in the search box
     driver.findElement(By.xpath("//input[@type='search']")).sendKeys("Auto Testing");
     Thread.sleep(3000);
     Action.screencapture();
	 Action.TestLog(" Enter Group name in the search box");
//   Click on the edit button
     driver.findElement(By.xpath("//button[@type = 'submit']")).click();
     Thread.sleep(3000);
     Action.screencapture();
	 Action.TestLog("Click on the edit button");
//   clear group name
     driver.findElement(By.xpath("//input[@id='groupName']")).clear();
     Thread.sleep(3000);
     Action.screencapture();
	 Action.TestLog("clear group name");
//   Edit group name
     driver.findElement(By.xpath("//input[@id='groupName']")).sendKeys("Auto Testing 1");
     Action.screencapture();
	 Action.TestLog(" Edit group name");
//   Click on the save button
     driver.findElement(By.xpath("//input[@value='Save']")).click();
     WebElement suceessTost = driver.findElement(By.xpath("//*[@id=\"attendanceGroup\"]/div/div/div"));
     String ActualToast = suceessTost.getText();
     System.out.println(suceessTost.getText());
     String Expectedvalidation = "Attendance group updated successfully !!";
     Assert.assertEquals(ActualToast, Expectedvalidation);
//   Again Enter Group name in the search box
     driver.findElement(By.xpath("//input[@type='search']")).sendKeys("Auto Testing 1");
     Thread.sleep(3000);
//   Click on the delete button
     driver.findElement(By.xpath("//*[@id=\"datable_1\"]/tbody/tr[1]/td[3]/div/div[2]/form/button")).click();
     driver.switchTo().alert().accept();
     WebElement suceessTost1 = driver.findElement(By.xpath("//*[@id=\"attendanceGroup\"]/div/div/div"));
     String ActualToast1 = suceessTost1.getText();
     System.out.println(suceessTost1.getText());
     String Expectedvalidation1 = "Groupname deleted successfully !!";
     Assert.assertEquals(ActualToast1, Expectedvalidation1);
     Action.screencapture();
     
	 }

     
}



