package PageObject;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Action.Action;
import BaseClass.Baseclass;

public class AddBulkSalary extends Baseclass {

//	Initialization
	public AddBulkSalary() {
		PageFactory.initElements(driver, this);
	}

//	Deceleration

//  Click on the new HR
	@FindBy(xpath = "//a[normalize-space()='New HR']")
	WebElement NewHR;

//    Click on the add bulk salary
	@FindBy(xpath = "//a[@href='/getepayPortal/addBulkEmployeeSalary']")
	WebElement AddBulkSalary;

//    Click on the select grade name
	@FindBy(xpath = "//span[@role='combobox']")
	WebElement SelectGradeName;

//    Enter grade name in the search box
	@FindBy(xpath = "//input[@aria-label='Search']")
	WebElement Entergradename;

//    Click on the download sample file
	@FindBy(xpath = "//button[normalize-space()='Download Sample File']")
	WebElement Downloadsamplefile;
	
	@FindBy(id = "uploadFile")
	WebElement upload_input_Box;

	@FindBy(id = "DataSubmit")
	WebElement Upload_Button;

	@FindBy(id = "messageBox")
	public WebElement toast;

	@FindBy(id = "messageBox")
	WebElement ValidationMessage;
	
//    Usage

	public void clickNewHR() {
		Action.TestLog("Clicked on NewHR Button.");
		NewHR.click();
	}

//    Add bulk salary
	public void addBulkSalary() {
		Action.TestLog("Add bulk salary");
		AddBulkSalary.click();
	}

//    Click on the select grade name
	public void SelectGradeName1() {
		Action.TestLog("Add bulk salary");
		SelectGradeName.click();
	}

//  Enter grade name in the search box
	public void Entergradename1() {
		Action.TestLog("Enter grade name in the search box");

		String gradeName = Add_Grade.GradeName;
		Entergradename.sendKeys(gradeName);
		Entergradename.sendKeys(Keys.ENTER);
	}

//    Click on the download sample file 

	public void Downloadsamplefile1() {
		Action.TestLog("Click on the download sample file ");
		Downloadsamplefile.click();
	}

	public void editsalarydetails() throws IOException {

		int ctc = AddSalaryDetails.ctc;
		String[] SlabPercentage = Add_Grade.GradeValue;
		List<Integer> slabsPercentage = new ArrayList<Integer>();

		for (String slab : SlabPercentage) {
			int Slab = Integer.valueOf(slab);
			slabsPercentage.add(Slab);
		}

		int Basic = ctc * slabsPercentage.get(0) / 100;
		System.out.println(Basic);
		int Pf = ctc * slabsPercentage.get(1) / 100;
		System.out.println(Pf);
		int HRA = ctc * slabsPercentage.get(2) / 100;
		System.out.println(HRA);
		int TDS = ctc * slabsPercentage.get(3) / 100;
		System.out.println(TDS);
		int Conveyance = ctc * slabsPercentage.get(4) / 100;
		System.out.println(Conveyance);
		int Other = ctc * slabsPercentage.get(5) / 100;
		System.out.println(Other);
		int insurance = ctc * slabsPercentage.get(6) / 100;
		System.out.println(insurance);

		String path = "\\D:\\download\\salary.xlsx\"";

		FileInputStream excelFile = new FileInputStream(path);
		XSSFWorkbook workbook = new XSSFWorkbook(excelFile);
		Sheet sheet = workbook.getSheetAt(0);
		Row row = sheet.getRow(1);

		Cell cell1 = row.createCell(3);
		cell1.setCellValue(Basic);

		Cell cell2 = row.createCell(4);
		cell2.setCellValue(Pf);

		Cell cell3 = row.createCell(5);
		cell3.setCellValue(HRA);

		Cell cell4 = row.createCell(6);
		cell4.setCellValue(TDS);

		Cell cell5 = row.createCell(7);
		cell5.setCellValue(Conveyance);

		Cell cell6 = row.createCell(8);
		cell6.setCellValue(Other);

		Cell cell7 = row.createCell(9);
		cell7.setCellValue(insurance);

		FileOutputStream outputStream = new FileOutputStream(path);
		workbook.write(outputStream);
		outputStream.close();

	}

}
