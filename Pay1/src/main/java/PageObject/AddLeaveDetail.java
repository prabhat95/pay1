package PageObject;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import Action.Action;
import BaseClass.Baseclass;

public class AddLeaveDetail extends Baseclass {
	static Random random = new Random();

//	    Initialization
	public AddLeaveDetail() {
		PageFactory.initElements(driver, this);
	}

//      Declaration

//      Click on the new HR
	@FindBy(xpath = "//a[normalize-space()='New HR']")
	WebElement NewHR;

//      Click on the leave management
	@FindBy(xpath = "//a[normalize-space()='Leave Management']")
	WebElement ClickOnTheLeaveManagement;

//      Click on the add employee leave
	@FindBy(xpath = "//a[@href='/getepayPortal/addLeaveDetails']")
	WebElement AddEmployeeLeave;

//      Click Select employee
	@FindBy(xpath = "//span[@role='combobox']")
	WebElement ClickEmployee;

//      Search employee
	@FindBy(xpath = "//input[@aria-label='Search']")
	WebElement SearchEmployee;

//      Enter Privilege leaves
	@FindBy(xpath = "//tr[1]//td")
	List<WebElement> list;

	@FindBy(tagName = "tr")
	List<WebElement> rows;

//	Click on the save button
	@FindBy(xpath = "//input[@id='saveBtn']")
	WebElement SaveButton;
	
//	 Catch delete message
     @FindBy(xpath = "//*[@id=\"toasts\"]/div")
     WebElement Successmessage;

	int minValue = 1;
	int maxValue = 100;
	int randomNumber = random.nextInt(maxValue - minValue + 1) + minValue;
	
//   Usage

//        Click on the new HR
	public void clickNewHR() {
		Action.TestLog("Clicked on NewHR Button.");
		NewHR.click();
	}

//        Click on the leave management
	public void LeaveManagement() {
		Action.TestLog("Clicked on Leave Management Button.");
		ClickOnTheLeaveManagement.click();
	}

//          Click on the add employee leave
	public void addemployeeleave() {
		Action.TestLog(" Click on the add employee leave");
		AddEmployeeLeave.click();
	}

//  Click Select employee
	public void clickemployee1() {
		Action.TestLog(" Click on the add employee leave");
		ClickEmployee.click();
	}
//  Search employee
	public void seaerchemployee1() {
		SearchEmployee.sendKeys("Sandeep Chauhan");
		SearchEmployee.sendKeys(Keys.ENTER);
	}
	
	
//          Enter Privilege leaves
	public void enterleavedays() {
		
		List<WebElement> selectedRows = rows.subList(1, rows.size());
		
		for (WebElement row : selectedRows) {
			
			List<WebElement> cells = row.findElements(By.tagName("input"));
			
			for (int i = 1; i < cells.size(); i++) {
				
//				int Number = random.nextInt(10, 30);
				String days = String.valueOf(randomNumber);
				cells.get(i).clear();
				cells.get(i).sendKeys(days);

			
		}}}
			
//	        Click on the save button
			public void savebutton() {
				SaveButton.click();
			}
		
			
//			Success message
			public void ConfirmsuccessMessage1() {

		    	   Action.TestLog("Leave Update Successfully !!");
		          	String ActualToast = Successmessage.getText();
		      	     System.out.println(Successmessage.getText());
		      	     String Expectedvalidation = "Leave Update Successfully !!";
		      	     Assert.assertEquals(ActualToast, Expectedvalidation);
		          
		       }
   
		
		}
