package PageObject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import BaseClass.Baseclass;

public class product_Category_Page extends Baseclass{

	public product_Category_Page() {
		// TODO Auto-generated constructor stub
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath ="//span[@class='imformative-title']")
	WebElement Header_Of_the_Page;
	@FindBy(id="selectType")
	WebElement select_Product_Type;
	@FindBy(id="prodCategory")
	WebElement Select_Product_Category_Drowdown;
	@FindBy(id="prodCategory")
	WebElement prodCategory_DropDown;
	@FindBy(id="brandnameIdentifier")
	WebElement brandnameIdentifier_Dropdown;
	@FindBy(id="serialNo")
	WebElement serialNo_Input;
	@FindBy(xpath = "//input[@name='warranty']")
	WebElement warranty_Input;
	@FindBy(id="remarks")
	WebElement remarks_Input;
	@FindBy(id="quantity")
	WebElement Enter_quantity_Input;
	@FindBy(xpath="//input[@name='vendor']")
	WebElement vandor_Input;
	@FindBy(id= "addAssetSaveBtn")
	WebElement addAssetSaveBtn;
	@FindBy(xpath="//button[@class='btn pl-3']")
    WebElement Download_Sampe_Product_Sheet;
	@FindBy(id= "file")
	WebElement Upload_File;
	@FindBy(id= "updateAssetSaveBtn")
	WebElement Uploaded_File_Save_Button;
	
}
