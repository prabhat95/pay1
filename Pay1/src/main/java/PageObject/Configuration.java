package PageObject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import Action.Action;
import BaseClass.Baseclass;


public class Configuration extends Baseclass{

//  Initialization
    public Configuration() {
    PageFactory.initElements(driver,this);
    }
	
	
	
//  declaration
    
//    Enter email
    @FindBy(xpath = "//input[@placeholder='Enter user name']")
    WebElement Login;
//    Enter Password
    @FindBy(xpath = "//input[@id='password']")
    WebElement Password;
//    CLick on the login button
    @FindBy(xpath = "//button[@type='submit']")
    WebElement LoginClick;
//    Click on the new HR
    @FindBy(xpath="//a[normalize-space()='New HR']")
    WebElement NewHR ;	
//    Click on the attendance
    @FindBy(xpath="//a[normalize-space()='Attendance']")
	WebElement Attendence;
//   Click on the configuration
    @FindBy(xpath = "//a[normalize-space()='Configuration']")
    WebElement Configuration;
//   Choose Group 
    @FindBy(xpath = "//select[@id='groupId']")
    WebElement EnterGroupName;
//    Select type
    @FindBy(xpath = "//select[@id='shift']")
    WebElement SelectType;
//   Select working shift
    @FindBy(xpath =  "//select[@id='shiftId']")
    WebElement SelectWorkingShift;
//  Select Employee Name
    @FindBy(xpath = "//select[@id='selectEmp']")
    WebElement SelectEmployeeName;
//   Select working days
    @FindBy(xpath = "//select[@id='workingDays']")
    WebElement SelectWorkingDays;
//    Click on the save button
    @FindBy(xpath = "//input[@value='Save']")
    WebElement SaveButton;
//	Catch success Message
	@FindBy(xpath = "/html/body/div/div/div[2]/div[2]/div[1]/div")
    WebElement Successmessege;
    
//    Usage 
    
//  Enter email 
    public void Login() {
    	Action.TestLog("Enter Valid email Id or username");
    	Login.sendKeys("Test_merchant");
    }
//    Enter password
    public void Password() {
    	Action.TestLog("Enter Valid password");
    	Password.sendKeys("Test@123");
    }
//    Click on the login button
    public void ClicLoginButton() {
    	Action.TestLog("Clicked on Login Button.");
    	LoginClick.click();
    }
//	   Click on the new HR
    public void clickNewHR() {
    	Action.TestLog("Clicked on NewHR Button.");
    	NewHR.click();
    }
    
//    Click on the attendance
    public void clickAttendenceBtn() {
    	Action.TestLog("Clicked on Attendence Button.");
    	Attendence.click();
    }
    
//    Click configuration
    public void clickOnTheConfiguration() {
    	Action.TestLog("Clicked on the configuration Button.");
    	Configuration.click();
    }
//    Choose group 
    public void EnterGroupName() {
    	Action.TestLog("Choose Group Name.");
    	Select Sel = new Select (EnterGroupName);
		Sel.selectByIndex(3);
    }
//    Select Type
    public void SelectType() {
    	Action.TestLog("Choose Select Type.");
    	Select Sel = new Select (SelectType);
		Sel.selectByIndex(1);
    }
//    Select working shift
    public void SelectWorkingShift() {
    	Action.TestLog("Choose working shift.");
    	Select Sel = new Select (SelectWorkingShift);
		Sel.selectByIndex(1);
    }
//    Select Employee Name
    public void SelectEmployeeName() {
    	Action.TestLog("Choose Employee Name.");
    	Select Sel = new Select (SelectEmployeeName);
		Sel.selectByIndex(1);
		Sel.selectByIndex(2);
    }
//    Select working days
    public void SelectWorkingDays() {
    	Action.TestLog("Choose working days.");
    	Select Sel = new Select (SelectWorkingDays);
		Sel.selectByIndex(1);
		Sel.selectByIndex(2);
    }
//    Click on the save button
    public void SaveButton() {
    	Action.TestLog("Clicked on the Save Button.");
    	SaveButton.click();
    }
//	Catch success Message
    public void Successmessege() {
    	Action.TestLog("Attendance configuration saved successfully !!.");
    	String ActualToast = Successmessege.getText();
	     System.out.println(Successmessege.getText());
	     String Expectedvalidation = "Attendance configuration saved successfully !!";
	     Assert.assertEquals(ActualToast, Expectedvalidation);
    }
    
}
