package PageObject;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import Action.Action;
import BaseClass.Baseclass;

public class Mark_Attendance extends Baseclass {

	public static String expectedMonth = "SEP 2023";
	public static String dateOne = "1";
	public static String dateTwo = "30";

	public void mark_Attendance1(char initial1, char initial2) throws InterruptedException {

		Mark_Attendance1.click();
		Thread.sleep(1000);
		MarkAttendance2.click();
		Thread.sleep(1000);
		String EmployeeCOunt = EmployeeCards.getText();
		System.out.println("Number of Employee appeared : " + EmployeeCOunt);
		Select atendance = new Select(attendanceDropdown);
		String selectedAttendance = atendance.getFirstSelectedOption().toString();

		String[] Atendance = selectedAttendance.split(" ");
		int index = Atendance.length;

		if (index == 2) {
			initial1 = Atendance[0].charAt(0);
			initial2 = Atendance[0].charAt(1);
		} else {
			initial1 = Atendance[0].charAt(0);
		}

		String empname = employeeName.getText();
		System.out.println("Employee name : " + empname + " Attendance type choosen : + " + selectedAttendance);
		Thread.sleep(800);
		Save_Btn.click();
		Action.waitForVisible(driver, Validation_Message);
		String ToastMessage = Validation_Message.getText();
		System.out.println("Toast message Displayed : " + ToastMessage);
		boolean toast = Validation_Message.isDisplayed();
		Assert.assertEquals(toast, true);

	}

	public void Update_Attendance(char initial1, char initial2 ) throws InterruptedException {

		Mark_Attendance1.click();
		Thread.sleep(1000);
		upadate_Attendance.click();
		Thread.sleep(1000);

		Select_date_Range.click();

		calendar(expectedMonth, dateOne, dateTwo);
		Thread.sleep(1000);

		System.out.println("Selected date to update attendance" + Select_date_Range.getText());

		Select_employee.click();
		Thread.sleep(500);

		searchEmployee.sendKeys("Anirudh");
		searchEmployee.sendKeys(Keys.ENTER);

		Thread.sleep(800);

		String selectedEmployee = selected_Employee.getText();
		System.out.println("Employee selected : " + selectedEmployee);

		search_Btn.click();

		String date = Action.Current_Date("yyyy-mm-dd");

		search_Input.sendKeys(date);

		int a = initial1 + initial2;
		String atendanceType = String.valueOf(a);
		System.out.println("Attendance in update Attendance page : " + atendanceType);
		String atendance = Attendance_On_Current_day.getText();

		boolean Updated_Attendance = atendance.equalsIgnoreCase(atendanceType);

		Assert.assertEquals(Updated_Attendance, true);

		System.out.println(row.getText());

		EditButton.click();
		Thread.sleep(1000);

		Select sel1 = new Select(SelectAttendanceToUpdate);
		sel1.selectByIndex(1);
		String updatedAttendance = sel1.getFirstSelectedOption().toString();
		System.out.println("Updated Attendace of employee :" + updatedAttendance);
		Thread.sleep(1000);

		Update_Btn.click();
		Thread.sleep(1000);

		String validation = Validation_Message_UploadAttendance.getText();
		System.out.println("Toast Message displayed : " + validation);

	}

	public void upload_Attendance_Sheet(String employee, String Update1, String Update2 , String attendance1,String attendance2) throws InterruptedException, IOException, InvalidFormatException {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		Action.waitForVisible(driver, Mark_Attendance1);

		Mark_Attendance1.click();

		Action.waitForVisible(driver, attendance_Sheet);

		attendance_Sheet.click();

		Action.waitForVisible(driver, driver.findElement(By.id("dateInput")));

		WebElement Select_date_Range1 = driver.findElement(By.id("dateInput"));

		Select_date_Range1.click();

		calendar(expectedMonth, dateOne, dateTwo);

		Action.waitForVisible(driver, Select_employee);

		Select_employee.click();

		searchEmployee.sendKeys(employee);

		searchEmployee.sendKeys(Keys.ENTER);

		Download_Btn.click();

		String path = "/home/prabhat/git/repository2/Pay1/Downloaded_Pdf/Attendance_Sheet.csv";

		try {

			// Step 1: Read the data from the CSV file
			CSVReader reader = new CSVReader(new FileReader(path));

			List<String[]> lines = reader.readAll();
			reader.close();

			// Step 2: Update the data in memory
			for (String[] line : lines) {

				if (line[0].equalsIgnoreCase("EmployeeCode")) {

					System.out.println("Date at third index : " + line[3] + "  Date at 10 index : " + line[10]);

					Update1 = line[3];
					Update2 = line[10];
				} else {

					line[3] = attendance1;
					line[10] = attendance2;
				}
			}

			// Step 3: Write the updated data back to the CSV file
			CSVWriter writer = new CSVWriter(new FileWriter(path));

			writer.writeAll(lines);
			writer.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		upload_File.sendKeys(path);

		Submit_Btn.click();

		String ActualValidation = Validation_Message_UploadAttendance.getText();
		String ExpectedValidation = "Attendance Update Successfully";

		Assert.assertEquals(ActualValidation, ExpectedValidation);

	}

	public void Verify_Updated_attendance(String employee,String Update1,String attendance1,String Update2) throws InterruptedException {

		List<String> attendanceData = new ArrayList<String>();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		Mark_Attendance1.click();

		upadate_Attendance.click();

		Action.waitForVisible(driver, Select_date_Range);

		Select_date_Range.click();

		calendar(expectedMonth, dateOne, dateTwo);

		System.out.println("Selected date to update attendance" + Select_date_Range.getText());

		Select_employee.click();

		searchEmployee.sendKeys(employee);

		searchEmployee.sendKeys(Keys.ENTER);

		String selectedEmployee = selected_Employee.getText();

		System.out.println("Employee selected for the test : " + selectedEmployee);

		search_Btn.click();

		Update1 = Update1.substring(0, 10);
		Update2 = Update2.substring(0, 10);

		search_Input.sendKeys(Update1);

		Thread.sleep(2000);
		WebElement row = driver.findElement(By.xpath("//tbody//tr"));
		List<WebElement> cells = row.findElements(By.tagName("td"));

		for (int i = 0; i < cells.size(); i++) {

			String cellValue = cells.get(i).getText();

			attendanceData.add(cellValue);
			System.out.println(cellValue);

		}

		try {
			System.out.println("Employee name : " + attendanceData.get(1));
			boolean Eployeename = attendanceData.get(1).equalsIgnoreCase(employee);
			Assert.assertEquals(Eployeename, true);

			System.out.println("updated Attendance is : " + attendanceData.get(2));
			boolean Attendance = attendanceData.get(2).equalsIgnoreCase(attendance1);
			Assert.assertEquals(Attendance, true);

			System.out.println("Attendance updated of the day : " + attendanceData.get(3));
			boolean dateOfAttendance1 = attendanceData.get(3).contains(Update1);
			Assert.assertEquals(dateOfAttendance1, true);

		} catch (AssertionError e) {
			e.printStackTrace();
		}

		EditButton.submit();

		Action.waitForVisible(driver, AttendanceList);

		AttendanceList.click();

		searchEmployee.sendKeys("Present");
		System.out.println("                Present Marked.");
		searchEmployee.sendKeys(Keys.ENTER);

		Thread.sleep(1000);

		search_Btn.click();

	}

	public void calendar(String expectedMonth,String dateOne, String dateTwo) throws InterruptedException {

		Action.waitForVisible(driver, driver.findElement(By.xpath("(//th[@class='month'])[1]")));

		WebElement month1 = driver.findElement(By.xpath("(//th[@class='month'])[1]"));
		String Month = month1.getText();

		while (!Month.equalsIgnoreCase(expectedMonth)) {

			month1 = driver.findElement(By.xpath("(//th[@class='month'])[1]"));
			Month = month1.getText();
			System.out.println(">> " + Month);

			if (Month.equalsIgnoreCase(expectedMonth)) {

				break;
			}
			Action.JSClick(driver, Select_previoud_Month);
		}

		for (WebElement calendar : calendars) {

			WebElement monthHead = calendar.findElement(By.xpath("//th[@class='month']"));

			String monthName = monthHead.getText();
			System.out.println("Month : " + monthName);

			if (monthName.equalsIgnoreCase(expectedMonth)) {

				List<WebElement> Calendar_Date = calendar.findElements(By.xpath("//table//tbody//tr//td"));

				Thread.sleep(2000);
				// Select first date
				for (int i = 0; i < Calendar_Date.size(); i++) {

					String date1 = Calendar_Date.get(i).getText();

					if (date1.equals(dateOne)) {

						System.out.println("Enter in the block 1 & date matched : " + date1);
						WebElement dateToClick = Calendar_Date.get(i);
						System.out.println("i = " + i);
						Action.waitForVisible(driver, dateToClick);

						dateToClick.click();

						Action.implicitWait(driver, 10);

						for (int j = i; j < Calendar_Date.size(); j++) {

							Calendar_Date = calendar.findElements(By.xpath("//table//tbody//tr//td"));

							String date2 = Calendar_Date.get(j).getText();
							Thread.sleep(200);

							if (date2.equals(dateTwo)) {

								System.out.println("Entered in the & date2 matched : " + date2);

								Action.waitForVisible(driver, Calendar_Date.get(j));
								Calendar_Date.get(j).click();

								Action.waitForVisible(driver,
										driver.findElement(By.xpath("//div[@class='drp-buttons']//button[2]")));

								WebElement apply_Btn1 = driver
										.findElement(By.xpath("//div[@class='drp-buttons']//button[2]"));

								if (apply_Btn1.isDisplayed()) {
									Action.JSClick(driver, apply_Btn1);
								} else {
									apply_Btn1.click();
									break;
								}

							}
						}
					} else {
						continue;
					}

				}
			}
		}
	}

	public Mark_Attendance() {
		PageFactory.initElements(driver, this);
	}

	// MarkAttendance
	@FindBy(xpath = "(//a[@class='s-sidebar__nav-link'])[6]")
	WebElement Mark_Attendance1;
	@FindBy(xpath = "//ul[@class='dropdown-menu edit-ul show']//li[1]")
	WebElement MarkAttendance2;
	@FindBy(xpath = "(//select[@id='attendanceType'])[2]")
	WebElement Select_Attendance;
	@FindBy(xpath = "//input[@value='Save']")
	WebElement Save_Btn;
	@FindBy(xpath = "//*[@class='col-md-6 col-lg-4 custom-card-attendance']")
	WebElement EmployeeCards;
	@FindBy(xpath = "//div[@class='col-sm-12 api-response-message']")
	WebElement validation;
	@FindBy(id = "attendanceType")
	WebElement attendanceDropdown;
	@FindBy(xpath = "//span[@class='select2-selection select2-selection--single']")
	WebElement Select_Employee;
	@FindBy(xpath 	=	 "//div//input[@type='submit']")
	WebElement search_Btn;
	@FindBy(xpath = "//input[@type='search']")
	WebElement search_Input;
	@FindBy(xpath = "//td[3]")
	WebElement Attendance_column;
	@FindBy(xpath = "//td[4]")
	WebElement Attendance_date_time;
	@FindBy(xpath = "//button[@type='submit']")
	WebElement Edit_Btn;
	@FindBy(xpath = "(//*[@class='col-md-6 col-lg-4 custom-card-attendance'][2]//div)[1]//input")
	WebElement employeeName;
	@FindBy(id = "attendanceType")
	WebElement SelectAttendanceToUpdate;
	@FindBy(xpath = "//span[@role='combobox']")
	WebElement AttendanceList;
	// update an employee
	@FindBy(xpath = "//ul[@class='dropdown-menu edit-ul show']//li[2]")
	WebElement upadate_Attendance;
	@FindBy(xpath = "//select[@name='empID']")
	WebElement EmployeeName;
	@FindBy(xpath = "//span//input[@type='search']")
	WebElement searchEmployee;
	@FindBy(xpath = "//span[@class='select2-selection select2-selection--single']")
	WebElement Select_employee;
	@FindBy(xpath = "select2-data-attendanceType")
	WebElement Employee_Attendanc;
	@FindBy(xpath = "//input[@value='Update']") // button[@class='text-success btn']
	WebElement Update_Btn;
	@FindBy(id = "createdDate")
	WebElement Attendance_date;
	@FindBy(xpath = "//div[@class='col-sm-12 api-response-message']")
	WebElement Validation_Message;
	@FindBy(id = "daterange")
	WebElement Select_date_Range;
	@FindBy(xpath = "//div[@class='calendar-table']")
	List<WebElement> calendars;
	@FindBy(xpath = "((//div[@class='calendar-table'])[1]//th[1])[1]")
	WebElement Select_previoud_Month;
	@FindBy(xpath = "//div[@class='drp-buttons']//button[2]")
	WebElement apply_Btn;
	@FindBy(id = "select2-empId-container")
	WebElement selected_Employee;
	@FindBy(xpath = "//td[3]")
	WebElement Attendance_On_Current_day;
	@FindBy(tagName = "tr")
	WebElement row;
	@FindBy(xpath = "//button[@class='text-success btn']")
	WebElement EditButton;
	// Attendance Sheet
	@FindBy(xpath = "//ul[@class='dropdown-menu edit-ul show']//li[3]")
	WebElement attendance_Sheet;
	@FindBy(id = "employeeSelect")
	WebElement SelectEmployee;
	@FindBy(xpath = "//input[@value='Download']")
	WebElement Download_Btn;
	@FindBy(id = "file")
	WebElement upload_File;
	@FindBy(xpath = "(//div//button)[1]")
	WebElement Submit_Btn;
	@FindBy(xpath = "//div[@class='col-sm-12 api-response-message']")
	WebElement Validation_Message_UploadAttendance;
	@FindBy(xpath = "(//h5)[1]")
	WebElement Header_Of_Page;
	@FindBy(xpath = "(//h5)[2]")
	WebElement Header_Upload_functionality;

}
