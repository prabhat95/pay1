package PageObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.github.javafaker.Faker;

import BaseClass.Baseclass;

public class Add_Employee_Bulk extends Baseclass {

	static Faker faker = new Faker();
	static Random random = new Random();
	static Set<String> name1 = new LinkedHashSet<>();

	@FindBy(linkText = "Employee")
	WebElement Employee;

	@FindBy(linkText = "Employee Bulk Upload")
	WebElement Bulk_Upload;

	@FindBy(id = "uploadFile")
	WebElement Upload_file_input;

	@FindBy(id = "DataSubmit")
	WebElement Submit_Btn;

	@FindBy(id = "DisplayMessage")
	WebElement Validation;

	@FindBy(linkText = "Employee Details")
	WebElement EmployeeDetails;

	@FindBy(xpath = "//input[@type='search']")
	WebElement SearchEmployee;

	@FindBy(tagName = "tr")
	WebElement row;
	
	@FindBy(linkText = "Disable Employee")
	WebElement disable_Employee;

	static List<String> EmpList = new ArrayList<String>();
	int minValue = 1;
	int maxValue = 100;
	int randomNumber = random.nextInt(maxValue - minValue + 1) + minValue;

	public void Upload_Bulk_Employee(int n, int m) throws InterruptedException, IOException {
		String path = "/home/prabhat/git/repository2/Pay1/DataProvider/Book11.xlsx";

		FileInputStream fis = new FileInputStream(path);
		Workbook workbook = new XSSFWorkbook(fis);
		Sheet sheet = workbook.getSheetAt(0);

		int l = 5;
		int k = 8;

//		FileOutputStream fos = new FileOutputStream(path);

		Employee.click();
		Thread.sleep(1000);

		Bulk_Upload.click();
		Thread.sleep(1000);

		for (int i = l; i < k; i++) {

			Thread.sleep(1500);
			Row row = sheet.createRow(sheet.getLastRowNum() + 1);

			// Employee ID
//			int ID = random.nextInt(700, 1000);
			String EmpID = "GET" + randomNumber;
			Cell cell0 = row.createCell(0);
			cell0.setCellValue(EmpID);
			EmpList.add(EmpID);

			// Employee name
			String emplyName = getListOfNames().get(i);

			Cell cell1 = row.createCell(1);

			Cell previousCell = row.getCell(row.getLastCellNum() - 1);
			
			if (!emplyName.equalsIgnoreCase(previousCell.getStringCellValue())) {
				cell1.setCellValue(emplyName);
			}

			// Mobile Number
			Cell cell2 = row.createCell(2);
			cell2.setCellValue("0123456789");

			// Email ID
			Cell cell3 = row.createCell(3);
			cell3.setCellValue("test@test.in");

			// Department
			Cell cell4 = row.createCell(4);
			cell4.setCellValue("Technical");

			// Designation
			String desg = faker.job().position();
			Cell cell5 = row.createCell(5);
			cell5.setCellValue(desg);

			// UserType
			Cell cell6 = row.createCell(6);
			cell6.setCellValue("Office");

			// Reporting person
			Cell cell7 = row.createCell(7);
			cell7.setCellValue("Anirudh Pachisia");

			// Address
			Cell cell8 = row.createCell(8);
			cell8.setCellValue("93-94A, shyamNagar, Murlipura Shceme");

			// City
			Cell cell9 = row.createCell(9);
			cell9.setCellValue("Jaipur");

			// State
			Cell cell10 = row.createCell(10);
			cell10.setCellValue("Rajsthan");

			// Country
			Cell cell11 = row.createCell(11);
			cell11.setCellValue("India");

			// Pincode
			Cell cell12 = row.createCell(12);
			cell12.setCellValue("302001");

			// Geo Tracking
			Cell cell13 = row.createCell(13);
			cell13.setCellValue("1");

			// Joining
			String date = RandomDate();
			Cell cell14 = row.createCell(14);
			cell14.setCellValue(date);

			// Gradename
			Cell cell15 = row.createCell(15);
			cell15.setCellValue("TEAM LEADER");

			// DOB
			Cell cell16 = row.createCell(16);
			cell16.setCellValue("28/02/2000");

		}

		fis.close();
		FileOutputStream fos = new FileOutputStream(path);
		workbook.write(fos);
		fos.close();
		workbook.close();

		Upload_file_input.sendKeys("/home/prabhat/git/repository2/Pay1/DataProvider/Book11.xlsx");
		Thread.sleep(1000);

		Submit_Btn.click();

		if (Validation.isDisplayed()) {

			String valid = Validation.getText();
			System.out.println("Validatoin Diaplayed : " + valid);
		}

	}

	public void Verify_Newly_Added_Employees() throws InterruptedException {

		EmployeeDetails.click();
		Thread.sleep(1000);
		for (String employeeCode : EmpList) {

			SearchEmployee.sendKeys(employeeCode);

			WebElement cell2 = driver.findElement(By.xpath("//td[2]"));
			String EmpCode = cell2.getText();

			if (employeeCode.equalsIgnoreCase(EmpCode)) {

				System.out.println("Employee successfully addes via Bulk upload : ");
				System.out.println(row.getText());
				
			}

		}
	}
	
	public void Disable_an_Employee() throws InterruptedException {
		
		disable_Employee.click();
		Thread.sleep(1000);
		
		String emplID = EmpList.get(0);
		System.out.println("Disabling employee for employee ID : " + emplID);
		SearchEmployee.sendKeys(emplID);
		
		WebElement Disable_Btn = driver.findElement(By.xpath("//td[5]//form/button"));
		
		Disable_Btn.click();
		Thread.sleep(1000);
		
		SearchEmployee.sendKeys(emplID);
		Thread.sleep(500);
        
		String data = row.getText();
		System.out.println("Checking records After disabling an Employee : " + data);
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	// ________Date and Indian Name
	// Methods______________________________________________________________________________________

	public static String RandomDate() {
		// Create an instance of the Random class
		Random random = new Random();

		// Define a date range (start and end dates)
		Calendar startDate = new GregorianCalendar(2023, Calendar.JULY, 1);
		Calendar endDate = new GregorianCalendar(2023, Calendar.SEPTEMBER, 30);

		// Calculate the difference in milliseconds between the start and end dates
		long startMillis = startDate.getTimeInMillis();
		long endMillis = endDate.getTimeInMillis();
		long randomMillis = startMillis + (long) (random.nextDouble() * (endMillis - startMillis));

		// Create a Calendar instance with the random date
		Calendar randomDate = new GregorianCalendar();
		randomDate.setTimeInMillis(randomMillis);

		// Extract year, month, and day from the random date
		int year = randomDate.get(Calendar.YEAR);
		int month = randomDate.get(Calendar.MONTH) + 1; // Month is 0-based, so add 1
		int day = randomDate.get(Calendar.DAY_OF_MONTH);

		String inty = String.valueOf(year);
		String intm = String.valueOf(month);
		String intd = String.valueOf(day);

		String date;

		if (intm.equalsIgnoreCase("11") || intm.equalsIgnoreCase("10") || intm.equalsIgnoreCase("12")) {
			date = intd + "/" + intm + "/" + inty;
			System.out.println("return Date : " + date);

		} else {
			date = intd + "/" + "0" + intm + "/" + inty;
			System.out.println("return Date : " + date);

		}

		return date;

	}

	public static List<String> getListOfNames() throws IOException {
//
//		String path = "/home/prabhat/git/repository/GetePay/DataProvider/BulkMUpload.xlsx";
//		FileInputStream fis = new FileInputStream(path);
//		Workbook workbook = new XSSFWorkbook(fis);
//		Sheet sheet = workbook.getSheetAt(0);

		FileOutputStream fos = null;
		File file = new File("/home/prabhat/git/repository2/Pay1/DataProvider/IndianOracle.com-Baby-Names.pdf");

		String path1 = file.getPath();

		PDDocument document = PDDocument.load(new File(path1));

		PDFTextStripper pdfTextStripper = new PDFTextStripper();

		String pdfContent = pdfTextStripper.getText(document);

		String[] lines = pdfContent.split("\\r?\\n");
		int h = 1;
		for (String line : lines) {

			if (line.contains("=")) {

				String[] h2 = line.split("=");
				System.out.println(h + ". " + h2[0]);
				h++;
				name1.add(h2[0]);

			}
		}
		int j = 1;
		System.out.println("Number of Names in list : " + name1.size());
		for (String name : name1) {

			System.out.println(j + "." + name);

			j++;
			if (j == 3) {
				break;
			}

		}

		List<String> uniqueNames = new ArrayList<>(name1);
		System.out.println("21th name in the list : " + uniqueNames.get(20));

		return uniqueNames;
	}

}
