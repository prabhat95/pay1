package PageObject;

import javax.swing.JOptionPane;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import Action.Action;
import BaseClass.Baseclass;

public class AddLeave extends Baseclass {

//	Initialization
	public AddLeave() {
		PageFactory.initElements(driver, this);
	}

//     Declaration

//  Click on the new HR
	@FindBy(xpath = "//a[normalize-space()='New HR']")
	WebElement NewHR;

//      Click on the leave management
	@FindBy(xpath = "//a[normalize-space()='Leave Management']")
	WebElement ClickOnTheLeaveManagement;

//      Click on the add leave
	@FindBy(xpath = "//a[@href='/getepayPortal/mweb/loadServices?m=getAttendanceLeave']")
	WebElement ClickOnTheAddLeave;

//     Enter Leave Type
	@FindBy(xpath = "//input[@id='leaveType']")
	WebElement EnterLeaveType;

//     Click on the save button
	@FindBy(xpath = "//input[@value='Save']")
	WebElement ClickOnTheSaveButton;

//       Success message
	@FindBy(xpath = "//*[@id=\"attendanceLeave\"]/div/div")
	WebElement CatchSuceessMessage;

//     Leave Type in the search box
	@FindBy(xpath = "//input[@type='search']")
	WebElement EntertextinSearchBox;

//     Click on the delete button
	@FindBy(xpath = "//button[@type='submit']")
	WebElement ClickOnTheDeleteButton;

//     Catch delete message
	@FindBy(xpath = "//*[@id=\"attendanceLeave\"]/div/div")
	WebElement ConfirmDeleteMessage;

//       Usage

//	   Click on the new HR
	public void clickNewHR() {
		Action.waitForVisible(driver, NewHR);
		Action.TestLog("Clicked on NewHR Button.");
		NewHR.click();
	}

//    Click on the leave management
	public void LeaveManagement() {
		Action.waitForVisible(driver, ClickOnTheLeaveManagement);
		Action.TestLog("Clicked on Leave Management Button.");
		ClickOnTheLeaveManagement.click();
	}

//     Click on the add leave
	public void AddLeave1() {
		Action.waitForVisible(driver, ClickOnTheAddLeave);
		Action.TestLog("Clicked on Add Leave Button.");
		ClickOnTheAddLeave.click();
	}

//       Enter Leave Type
	public void LeaveType1() {
		Action.TestLog("Enter Leave Type");
		String str = JOptionPane.showInputDialog("Enter Leave name");
		EnterLeaveType.sendKeys(str);
	}

//      Click on the save button
	public void ClickOnTheSaveButton1() {
		ClickOnTheSaveButton.click();
	}

//    Success message
	public void CatchSuceessMessage1() {
		Action.TestLog("Leave-Type saved successfully !!");
		String ActualToast = CatchSuceessMessage.getText();
		System.out.println(CatchSuceessMessage.getText());
		String Expectedvalidation = "Leave-Type saved successfully !!";
		Assert.assertEquals(ActualToast, Expectedvalidation);
	}

//       Leave Type in the search box
	public void EnterLeaveTypeInSearchBox1() {
		String str = JOptionPane.showInputDialog("Enter shift name");
		EntertextinSearchBox.sendKeys(str);
	}

//        Click on the delete button
	public void ClickOnTheDeleteButton1() {
		ClickOnTheDeleteButton.click();
	}

//       Catch delete message
	public void ConfirmDeleteMessage1() {
		Action.TestLog("Leave-Type deleted successfully !!");
		String ActualToast = ConfirmDeleteMessage.getText();
		System.out.println(ConfirmDeleteMessage.getText());
		String Expectedvalidation = "Leave-Type deleted successfully !!";
		Assert.assertEquals(ActualToast, Expectedvalidation);

	}

}