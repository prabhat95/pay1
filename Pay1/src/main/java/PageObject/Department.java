package PageObject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import BaseClass.Baseclass;

public class Department extends Baseclass {

	public Department() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "departmentName")
	WebElement Department_Name;
	@FindBy(id= "saveDepartment")
	WebElement save_Department;
	@FindBy(xpath= "//div[@class='text-center api-response-message']")
	WebElement validationMessage;
	@FindBy(id="departmentSet")
	WebElement DepartmentDroopDown;
    @FindBy(id="designationName")
    WebElement DesignationName;
    @FindBy(id="saveDesignatioin")
    WebElement saveDesignatioin;
    
	
	
	
}
