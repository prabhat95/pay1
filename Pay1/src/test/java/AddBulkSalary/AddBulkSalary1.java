package AddBulkSalary;

import org.testng.annotations.Test;
import java.io.IOException;


import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Action.Action;
import BaseClass.Baseclass;
import PageObject.AddBulkSalary;
import PageObject.Pay1Login;

public class AddBulkSalary1 extends Baseclass {
	
	Pay1Login pay1login;
	AddBulkSalary addbulksalary;

	
	@Test (groups = {"Regression"})
	
	public void AddBulkSalary() throws InterruptedException, IOException {
		
		pay1login = new Pay1Login();
		addbulksalary = new AddBulkSalary();

		pay1login.Enteremail();
		Thread.sleep(1000);
		
		pay1login.enterPassword();
		Thread.sleep(1000);
		
		pay1login.clickSubmitButton();
		Thread.sleep(1000);
		
		addbulksalary.clickNewHR();
		Action.screencapture();
		Thread.sleep(1000);
		
		addbulksalary.addBulkSalary();
		Action.screencapture();
		Thread.sleep(1000);
		
		addbulksalary.SelectGradeName1();
		Action.screencapture();
		Thread.sleep(1000);
		
		addbulksalary.Entergradename1();
		Action.screencapture();
		Thread.sleep(1000);
		
		addbulksalary.Downloadsamplefile1();
		Action.screencapture();
		Thread.sleep(1000);
		
		addbulksalary.Entergradename1();
		

	}

	@BeforeTest(alwaysRun = true)
	public void launchBrowser() throws InterruptedException {
		launchApp();

	}

	@AfterTest(alwaysRun = true)
	public void exittest() {
		driver.quit();
	}

}
