package pay1.UploadHolydays;

import org.testng.annotations.Test;
import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseClass.Baseclass;
import PageObject.Pay1Login;
import PageObject.Upload_Holyday;

public class upload_Holyday extends Baseclass {

	Pay1Login dashboard;
	Upload_Holyday holyday;

	@Test(groups = {"Regression"})
	public void Upload_Holyday_Test() throws IOException, InterruptedException {

		dashboard = new Pay1Login();
		
		holyday = new Upload_Holyday();

		dashboard.Pay1_Login();
		
		dashboard.Click_On_New_HR();

		dashboard.Click_On_Upload_Holydays();

		holyday.Upload_Holydays();

	}

	@AfterMethod(alwaysRun = true)
	public void StopBrowser() {
		driver.close();

	}

	@BeforeMethod(alwaysRun = true)
	public void StartBrowser() {
		launchApp();

	}

}
