package Add_Employee;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseClass.Baseclass;
import PageObject.AddEmployees;
import PageObject.Add_Employee_Bulk;
import PageObject.IndianFirstNameGenerator;

public class AddEmployee extends Baseclass {

	AddEmployees hr;
	Add_Employee_Bulk bulk;

	IndianFirstNameGenerator nam;

	int m = 0;
	int n = 2;

	int k = 5;
	int l = 8;

	@Test(groups = {"Regression"})

	public void AddEmployes() throws InterruptedException, IOException {

		hr = new AddEmployees();

		Thread.sleep(1000);
		hr.newHR(n, m);

	}

	@Test(description = "Verifying that user is able to Upload Employee in Bulk and "
			+ "Checking that employees in employee list.")
	public void Bulk_Employee_Upload() throws InterruptedException, IOException {

		bulk = new Add_Employee_Bulk();

		bulk.Upload_Bulk_Employee(k, l);

		bulk.Verify_Newly_Added_Employees();

	}

	@BeforeMethod(alwaysRun = true)
	public void beforeMethod() {
		launchApp();
	}

	@AfterMethod(alwaysRun = true)
	public void afterMethod() {
		driver.quit();
	}

}
