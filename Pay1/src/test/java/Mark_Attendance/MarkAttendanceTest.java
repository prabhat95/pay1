package Mark_Attendance;

import org.testng.annotations.Test;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseClass.Baseclass;
import PageObject.Mark_Attendance;
import PageObject.Pay1Login;

public class MarkAttendanceTest extends Baseclass {

	Mark_Attendance Mattendance;
	Pay1Login login;

	static char initial1 = 0;
	static char initial2 = 0;
	static String Update1 = " ";
	static String Update2 = " ";
	static String attendance1 = "A";
	static String attendance2 = "A";
	static String employee = "Anirudh";

	@Test (groups = {"Regression"})
	public void mark_Attendance1() throws InterruptedException, IOException, InvalidFormatException {

		Mattendance = new Mark_Attendance();
		login = new Pay1Login();

		login.Pay1_Login();

		login.Click_On_New_HR();

		Mattendance.mark_Attendance1(initial1, initial2);

	}
	
	@Test
	public void update_Attendance_Test() throws InterruptedException {
		
		Mattendance = new Mark_Attendance();
		
		Mattendance.Update_Attendance(initial1, initial2);
	}
	
	@Test (groups = {"Regression"}, priority=6)
	public void Upload_Attendance_Sheet_Test() throws InvalidFormatException, InterruptedException, IOException{
		
		Mattendance = new Mark_Attendance();
		
		Mattendance.upload_Attendance_Sheet( employee, Update1, Update2 ,attendance1, attendance2);
	}

	@Test
	public void Verify_updated_Attendance() throws InterruptedException {
		
		Mattendance = new Mark_Attendance();
		
		Mattendance.Verify_Updated_attendance(employee, Update1,attendance1, Update1);
	}
	
	@BeforeMethod(alwaysRun = true)
	public void LaunchApp() {
		
		launchApp();
	}

	@AfterMethod(alwaysRun = true)
	public void TearDown() {
		driver.quit();
	}

}
