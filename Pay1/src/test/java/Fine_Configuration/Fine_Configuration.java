package Fine_Configuration;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseClass.Baseclass;
import PageObject.FineConfiguration;
import PageObject.Pay1Login;

public class Fine_Configuration extends Baseclass {

	Pay1Login Dashboard;
	FineConfiguration fine;

	@Test(groups= {"Regression"})
	public void fine_Configuration_Test() throws InterruptedException {

		Dashboard = new Pay1Login();
		fine = new FineConfiguration();

		Dashboard.Pay1_Login();
		
		Thread.sleep(1000);

		Dashboard.Click_On_New_HR();

		Dashboard.Click_On_Fine_Configuration();

		fine.Fine_Configuration();

	}

	@AfterMethod
	public void Terdown() {

		driver.quit();

	}

	@BeforeMethod
	public void launchApp1() {

		launchApp();

	}

}
