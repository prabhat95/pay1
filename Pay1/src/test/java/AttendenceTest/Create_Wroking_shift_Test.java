package AttendenceTest;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseClass.Baseclass;
import PageObject.AddWorkingShift;



public class Create_Wroking_shift_Test extends Baseclass  {
	
	AddWorkingShift addshift;

	@Test (groups = {"Regression"})
	public void addWorkingShift() throws InterruptedException {
		
		addshift = new AddWorkingShift();
		
		Thread.sleep(1000);
		addshift.Login();
		Thread.sleep(3000);
		addshift.Password();
		Thread.sleep(3000);
		addshift.ClicLoginButton();
		Thread.sleep(3000);
		addshift.clickNewHR();
		Thread.sleep(3000);
		addshift.clickAttendenceBtn();
		Thread.sleep(3000);
		addshift.clickWorkingShift();
		Thread.sleep(3000);
		addshift.EnterShiftName();
		Thread.sleep(3000);
		addshift.EnterShiftTime();
		Thread.sleep(3000);
		addshift.ChooseIsDefault();
		Thread.sleep(3000);
		addshift.ClickSaveButton();
		addshift.CatchSuccessMessage();
		Thread.sleep(3000);
		addshift.SearchNameButton();
		Thread.sleep(3000);
		addshift.EditButton();
		Thread.sleep(3000);
		addshift.editToTheSelect();
		Thread.sleep(3000);
		addshift.ClickOnTheagainSaveButton();
		addshift.EditCatchsuccessmessage();
		Thread.sleep(1000);
		addshift.AgainSearchShiftName();
		Thread.sleep(3000);
		addshift.AgainClickOnTheEditButton();
		Thread.sleep(3000);
		addshift.EditToTheDefault();
		addshift.AgainClickOnTheSaveBtn();
		addshift.CatchDefaultSuccessMessage();
		Thread.sleep(3000);
	}
	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		driver.quit();
	}
	
	@BeforeMethod(alwaysRun = true)
	public void launchBrowser() throws InterruptedException {	
		launchApp();
	}
	
	
	
}
