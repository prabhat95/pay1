package AttendenceTest;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Action.Action;
import BaseClass.Baseclass;
import PageObject.Configuration;


public class Configuration1 extends Baseclass  {
      Configuration Config;
      
      @BeforeMethod(alwaysRun = true)
  	public void launchBrowser() throws InterruptedException {	
  		launchApp();
  	}
     @Test (groups = {"Regression"})
      public void configurationTest( ) throws InterruptedException, IOException {
    	  Config = new Configuration();
    	  
    	  Config.Login();
    	  Action.screencapture();
    	  Thread.sleep(1000);
    	  Config.Password();
    	  Action.screencapture();
    	  Thread.sleep(1000);
    	  Config.ClicLoginButton();
    	  Action.screencapture();
    	  Thread.sleep(1000);
    	  Action.screencapture();
    	  Config.clickNewHR();
    	  Action.screencapture();
    	  Thread.sleep(1000);
    	  Config.clickAttendenceBtn();
    	  Action.screencapture();
    	  Thread.sleep(1000);
    	  Config.clickOnTheConfiguration();
    	  Action.screencapture();
    	  Thread.sleep(1000);
    	  Config.EnterGroupName();
    	  Action.screencapture();
    	  Thread.sleep(1000);
    	  Config.SelectType();
    	  Action.screencapture();
    	  Thread.sleep(1000);
    	  Config.SelectWorkingShift();
    	  Action.screencapture();
    	  Thread.sleep(1000);
    	  Config.SelectEmployeeName();
    	  Action.screencapture();
    	  Thread.sleep(1000);
    	  Config.SelectWorkingDays();
    	  Action.screencapture();
    	  Thread.sleep(1000);
    	  Config.SaveButton();
    	  Action.screencapture();
    	  Config.Successmessege();
    	  Action.screencapture();
      }

     @AfterMethod(alwaysRun = true)
 	public void tearDown() {
 		driver.quit();
 	}















}
