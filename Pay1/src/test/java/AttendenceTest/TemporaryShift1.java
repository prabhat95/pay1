package AttendenceTest;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Action.Action;
import BaseClass.Baseclass;
import PageObject.TemporaryShift;

public class TemporaryShift1 extends Baseclass{
	      TemporaryShift tempshift;

	  @BeforeMethod
	  	public void launchBrowser() throws InterruptedException {	
		  launchApp();
	  	}	

	  @Test
      public void temporaryShift( ) throws InterruptedException, IOException {
    	  tempshift = new TemporaryShift();
	  tempshift.Login();
	  Action.screencapture();
	  Thread.sleep(1000);
	  tempshift.Password();
	  Action.screencapture();
	  Thread.sleep(1000);
	  tempshift.ClickLoginButton();
	  Action.screencapture();
	  Thread.sleep(1000);
	  tempshift.clickNewHR();
	  Action.screencapture();
	  Thread.sleep(1000);
	  tempshift.clickAttendenceBtn();
	  Action.screencapture();
	  Thread.sleep(1000);
	  tempshift.ClickOnTheTemporaryShift();
	  Action.screencapture();
	  Thread.sleep(1000);
	  tempshift.EmployeeName();
	  Action.screencapture();
	  Thread.sleep(1000);
	  tempshift.WorkingShift();
	  Action.screencapture();
	  Thread.sleep(1000);
	  tempshift.SelectDate();
	  Action.screencapture();
	  Thread.sleep(1000);
	  tempshift.SaveButtons();
	  Action.screencapture();
	  tempshift.SuccessfulMessege();
	  
	  
	  
	  }
	  
	  @AfterMethod
	 	public void tearDown() {
	 		driver.quit();
	 	}







}
