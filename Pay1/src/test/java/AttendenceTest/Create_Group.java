package AttendenceTest;

import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseClass.Baseclass;
import PageObject.AddWorkingShift;
import PageObject.CreateGroup;
import PageObject.Pay1Login;

public class Create_Group extends Baseclass{
	
	CreateGroup group;
	Pay1Login login;
	AddWorkingShift addshift ;
	
  @Test (groups = {"Regression"})
  public void CreateNewGroupTest() {
  
		login = new Pay1Login();
		addshift = new AddWorkingShift();

		login.Pay1_Login();

		login.Click_On_New_HR();
		
		addshift.clickAttendenceBtn();

	    group = new CreateGroup();
	  
	  
  }
  
  @BeforeMethod(alwaysRun = true)
  public void beforeMethod() {
  }

  @AfterMethod(alwaysRun = true)
  public void afterMethod() {
  }

}
