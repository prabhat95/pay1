package Generate_Salary_Sleep;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseClass.Baseclass;
import PageObject.Generate_Salary_Slip;
import PageObject.Pay1Login;

public class GenerateSalarySlipTest extends Baseclass {

	Generate_Salary_Slip salary;
	Pay1Login dashboard;

	String EmployeeName = "Anirudh";

	@Test (groups = {"Regression"})
	public void Salary_Genaration_Test() throws IOException, InterruptedException {

		dashboard = new Pay1Login();
		salary = new Generate_Salary_Slip();

		dashboard.Pay1_Login();
		
		dashboard.Click_On_New_HR();

		WebElement GeneratSalary = driver.findElement(By.xpath("//ul[@class='navbar-nav-items']//li[13]"));
		
		GeneratSalary.click();

		salary.Donwload_Salary_Slip(EmployeeName);

		salary.verifySalaryDetails();
	}

	@BeforeMethod(alwaysRun = true)
	public void startBrowser() {
		launchApp();
	}

	@AfterMethod(alwaysRun = true)
//	@AfterMethod
	public void tearDown() {
		driver.quit();

	}

}
