package LeaveManagement;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Action.Action;
import BaseClass.Baseclass;
import PageObject.AddLeaveDetail;
import PageObject.Pay1Login;

public class Addleavedetails extends Baseclass{
	             Pay1Login pay1login;
	             AddLeaveDetail addleavedetails;
	             
	             
	             @BeforeTest
	  	       public void launchBrowser() throws InterruptedException {	
	  	    		launchApp();
	  	}
	             @Test
	  	       public void leavedetails()throws InterruptedException, IOException {
	               
	            	 
	               addleavedetails = new AddLeaveDetail();
	  	           pay1login = new Pay1Login();
	  	           
	  	           
	  	           pay1login.Enteremail();
		           Thread.sleep(1000);
		           pay1login.enterPassword();
		           Thread.sleep(1000);
		           pay1login.clickSubmitButton();
		           Thread.sleep(1000);
		           addleavedetails.clickNewHR();
		           Action.screencapture();
		     	   Thread.sleep(1000);
		     	   addleavedetails.LeaveManagement();
		     	   Action.screencapture();
		     	   Thread.sleep(1000);
		     	   addleavedetails.addemployeeleave();
		     	   Action.screencapture();
		     	   Thread.sleep(1000);
		     	   addleavedetails.clickemployee1();
		     	   Action.screencapture();
		     	   Thread.sleep(1000);
		     	   addleavedetails.seaerchemployee1();
		     	   Action.screencapture();
		     	   Thread.sleep(5000);
		     	   addleavedetails.enterleavedays();
		     	   Action.screencapture();
		     	   Thread.sleep(5000);
		     	   addleavedetails.savebutton();
		     	   addleavedetails.ConfirmsuccessMessage1();
	             }    	  
		     	  
		     	  
//		     	 @AfterMethod
		     	public void tearDown() {
		     		driver.quit();
		     	}  
}
