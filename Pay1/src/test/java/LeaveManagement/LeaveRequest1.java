package LeaveManagement;

import java.io.IOException;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Action.Action;
import BaseClass.Baseclass;
import PageObject.LeaveRequest;
import PageObject.Pay1Login;

	public class LeaveRequest1 extends Baseclass {
		   LeaveRequest Leaverequest;
	       Pay1Login pay1login;
	       
	       @BeforeTest
	       public void launchBrowser() throws InterruptedException {	
	    		launchApp();
	}

	       @Test
	       public void addLeave1()throws InterruptedException, IOException {
	    	   Leaverequest = new LeaveRequest();
	           pay1login = new Pay1Login();
	   	     
	         
	           pay1login.Enteremail();
	           Thread.sleep(1000);
	           pay1login.enterPassword();
	           Thread.sleep(1000);
	           pay1login.clickSubmitButton();
	           Thread.sleep(1000);
	           Leaverequest.clickNewHR();
	           Action.screencapture();
	     	   Thread.sleep(1000);
	     	   Leaverequest.LeaveManagement();
	     	   Action.screencapture();
	    	   Thread.sleep(1000);
	    	   Leaverequest.LeaveRequestbutton1();
	    	   Action.screencapture();
	       	   Thread.sleep(1000);
	       	   Leaverequest.DateFieldBox1();
	       	   Leaverequest.selectedatefield1();
	       	   Action.screencapture();
	     	   Thread.sleep(1000);
               Leaverequest.selectedatefield2();
	       	   Leaverequest.ClickOnTheApplyButton1();
	       	   Action.screencapture();
	     	   Thread.sleep(1000);
	           Leaverequest.FilterData();
	           Action.screencapture();
	     	   Thread.sleep(1000);
	           Leaverequest.SelectEmployee1();
	           Action.screencapture();
	     	   Thread.sleep(1000);
	           Leaverequest.ClickOnTheFilterButton1();
	           Action.screencapture();
	     	   Thread.sleep(1000);
	           Leaverequest.ClickOnTheApproveButton();
	           Action.screencapture();
	     	   Thread.sleep(1000);
	     	  Leaverequest.SuccessMessage();
	     	   
	     	   
	       }
	       @AfterTest
	       public void exittest() {
	    	   driver.quit();
	       }
	       	   
	       	   
	       	   
	       	   
	       	   
	       	   
	       	   
	       	   
	       	   
	       	   
}
