package LeaveManagement;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Action.Action;
import BaseClass.Baseclass;
import PageObject.AddLeave;
import PageObject.Pay1Login;

public class Addleave_test extends Baseclass {

	AddLeave addleave;
	Pay1Login pay1login;

	@Test(groups = {"Regression"})
	public void addNewLeave() throws InterruptedException, IOException {
		
		addleave = new AddLeave();
		pay1login = new Pay1Login();

		pay1login.Pay1_Login();
		
		pay1login.Click_On_New_HR();
		
		Action.screencapture();
		Thread.sleep(1000);
		addleave.LeaveManagement();
		Action.screencapture();
		Thread.sleep(1000);
		addleave.AddLeave1();
		Action.screencapture();
		Thread.sleep(1000);
		addleave.LeaveType1();
		Action.screencapture();
		Thread.sleep(1000);
		addleave.ClickOnTheSaveButton1();
		Action.screencapture();

		addleave.CatchSuceessMessage1();
		Action.screencapture();

		addleave.EnterLeaveTypeInSearchBox1();
		Action.screencapture();
		Thread.sleep(1000);
		addleave.ClickOnTheDeleteButton1();
		Action.screencapture();

		addleave.ConfirmDeleteMessage1();
		Action.screencapture();
		Thread.sleep(1000);

	}

	@AfterMethod(alwaysRun = true)
	public void exittest() {
		
		driver.quit();
	}

	@BeforeMethod(alwaysRun = true)
	public void beforemethod() {
		
		launchApp();
	}

}
