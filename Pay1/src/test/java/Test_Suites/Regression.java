package Test_Suites;

import java.io.IOException;

import com.github.javafaker.Faker;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import org.testng.annotations.Test;

import Action.Action;
import BaseClass.Baseclass;
import PageObject.AddBulkSalary;
import PageObject.AddEmployees;
import PageObject.AddLeave;
import PageObject.AddSalaryDetails;
import PageObject.AddWorkingShift;
import PageObject.Add_Employee_Bulk;
import PageObject.Add_Grade;
import PageObject.Configuration;
import PageObject.FineConfiguration;
import PageObject.Generate_Salary_Slip;
import PageObject.Mark_Attendance;
import PageObject.Pay1Login;
import PageObject.Upload_Holyday;

public class Regression extends Baseclass {
	Pay1Login login;
	Add_Grade grade1;

	Faker faker = new Faker();
	String gradeName = faker.team().toString();
	@Test(groups = { "Regression" }, priority=2)
	public void Create_Grade_Test() throws InterruptedException {

		grade1 = new Add_Grade();

		grade1.Create_New_Grade( gradeName);

//		grade1.Delete_Grade(gradeName);

	}

	AddLeave addleave;
	Pay1Login pay1login;

	@Test(groups = { "Regression" }, priority=1)
	public void addNewLeave() throws InterruptedException, IOException {

		addleave = new AddLeave();

		addleave.LeaveManagement();
		Thread.sleep(1000);

		addleave.AddLeave1();
		Thread.sleep(1000);

		addleave.LeaveType1();
		Thread.sleep(1000);

		addleave.ClickOnTheSaveButton1();

		addleave.CatchSuceessMessage1();

		addleave.EnterLeaveTypeInSearchBox1();

		addleave.ClickOnTheDeleteButton1();

		addleave.ConfirmDeleteMessage1();
		Thread.sleep(1000);

	}

	AddEmployees hr;
	Add_Employee_Bulk bulk;

	@Test(groups = { "Regression" } , priority=3)

	public void AddEmployes() throws InterruptedException, IOException {

		hr = new AddEmployees();

		Thread.sleep(1000);
		hr.newHR(0, 2);

	}
	AddBulkSalary addbulksalary;

	@Test(groups = { "Regression" },priority = 7)

	public void AddBulkSalary() throws InterruptedException, IOException {

		addbulksalary = new AddBulkSalary();

		addbulksalary.addBulkSalary();
		Action.screencapture();
		Thread.sleep(1000);

		addbulksalary.SelectGradeName1();
		Action.screencapture();
		Thread.sleep(1000);

		addbulksalary.Entergradename1();
		Action.screencapture();
		Thread.sleep(1000);

		addbulksalary.Downloadsamplefile1();
		Action.screencapture();
		Thread.sleep(1000);

		addbulksalary.Entergradename1();

	}
	
	AddSalaryDetails addsalerydetails;   
  
       @Test (groups = {"Regression"}, priority = 6)
       public void addsalerydetails1() throws IOException, InterruptedException {
    	   addsalerydetails = new AddSalaryDetails();
    
    	   addsalerydetails.addsalerydetail1();
    	   Action.screencapture();
     	   Thread.sleep(1000);
    	   addsalerydetails.calender();
    	   Action.screencapture();
     	   Thread.sleep(1000);
    	   addsalerydetails.selectemployee();
    	   Action.screencapture();
     	   Thread.sleep(1000);
    	   addsalerydetails.searchemployee();
    	   Action.screencapture();
     	   Thread.sleep(1000);
    	   addsalerydetails.selectgrade(gradeName);
    	   Action.screencapture();
     	   Thread.sleep(1000);
    	   addsalerydetails.enterCTC();
    	   Action.screencapture();
     	   Thread.sleep(1000);
    	   addsalerydetails.savebutton();
    	   addsalerydetails.successmessage1();
       }
	
       Configuration Config;

      @Test (groups = {"Regression"}, priority = 5)
       public void configurationTest( ) throws InterruptedException, IOException {
     	  Config = new Configuration();
     	  
     	  Config.clickAttendenceBtn();
     	  Action.screencapture();
     	  Thread.sleep(1000);
     	  Config.clickOnTheConfiguration();
     	  Action.screencapture();
     	  Thread.sleep(1000);
     	  Config.EnterGroupName();
     	  Action.screencapture();
     	  Thread.sleep(1000);
     	  Config.SelectType();
     	  Action.screencapture();
     	  Thread.sleep(1000);
     	  Config.SelectWorkingShift();
     	  Action.screencapture();
     	  Thread.sleep(1000);
     	  Config.SelectEmployeeName();
     	  Action.screencapture();
     	  Thread.sleep(1000);
     	  Config.SelectWorkingDays();
     	  Action.screencapture();
     	  Thread.sleep(1000);
     	  Config.SaveButton();
     	  Action.screencapture();
     	  Config.Successmessege();
     	  Action.screencapture();
       }
	
      AddWorkingShift addshift;

  	@Test (groups = {"Regression"}, priority = 9 )
  	public void addWorkingShift() throws InterruptedException {
  		
  		addshift = new AddWorkingShift();
 
  		addshift.clickAttendenceBtn();
  		Thread.sleep(3000);
  		addshift.clickWorkingShift();
  		Thread.sleep(3000);
  		addshift.EnterShiftName();
  		Thread.sleep(3000);
  		addshift.EnterShiftTime();
  		Thread.sleep(3000);
  		addshift.ChooseIsDefault();
  		Thread.sleep(3000);
  		addshift.ClickSaveButton();
  		addshift.CatchSuccessMessage();
  		Thread.sleep(3000);
  		addshift.SearchNameButton();
  		Thread.sleep(3000);
  		addshift.EditButton();
  		Thread.sleep(3000);
  		addshift.editToTheSelect();
  		Thread.sleep(3000);
  		addshift.ClickOnTheagainSaveButton();
  		addshift.EditCatchsuccessmessage();
  		Thread.sleep(1000);
  		addshift.AgainSearchShiftName();
  		Thread.sleep(3000);
  		addshift.AgainClickOnTheEditButton();
  		Thread.sleep(3000);
  		addshift.EditToTheDefault();
  		addshift.AgainClickOnTheSaveBtn();
  		addshift.CatchDefaultSuccessMessage();
  		Thread.sleep(3000);
  	}
  	
  	FineConfiguration fine;

	@Test(groups= {"Regression"}, priority = 11)
	public void fine_Configuration_Test() throws InterruptedException {

		fine = new FineConfiguration();
	
		pay1login.Click_On_Fine_Configuration();

		fine.Fine_Configuration();

	}
	
	Generate_Salary_Slip salary;
	

	String EmployeeName = "Anirudh";

	@Test (groups = {"Regression"}, priority = 8)
	public void Salary_Genaration_Test() throws IOException, InterruptedException {

		salary = new Generate_Salary_Slip();

		WebElement GeneratSalary = driver.findElement(By.xpath("//ul[@class='navbar-nav-items']//li[13]"));
		
		GeneratSalary.click();

		salary.Donwload_Salary_Slip(EmployeeName);

		salary.verifySalaryDetails();
	}
	
	Mark_Attendance Mattendance;
	
	static char initial1 = 0;
	static char initial2 = 0;
	static String Update1 = " ";
	static String Update2 = " ";
	static String attendance1 = "A";
	static String attendance2 = "A";
	static String employee = "Anirudh";

	@Test (groups = {"Regression"} , priority = 4)
	public void mark_Attendance1() throws InterruptedException, IOException, InvalidFormatException {

		Mattendance = new Mark_Attendance();

		Mattendance.mark_Attendance1(initial1, initial2);

	}
	
	Upload_Holyday holyday;

	@Test(groups = {"Regression"}, priority = 10)
	public void Upload_Holyday_Test() throws IOException, InterruptedException {

		
		holyday = new Upload_Holyday();

		pay1login.Click_On_Upload_Holydays();

		holyday.Upload_Holydays();

	}


	@BeforeClass
	public void beforeClass() {
		launchApp();
	}

	@AfterClass
	public void afterClass() {

		driver.quit();
	}

}
