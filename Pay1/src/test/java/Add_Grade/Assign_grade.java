package Add_Grade;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseClass.Baseclass;
import PageObject.Add_Grade;
import PageObject.Pay1Login;

public class Assign_grade extends Baseclass {

	Pay1Login login;
	Add_Grade grade1;

	@Test(groups = {"Regression"})
	public void Create_Grade_Test() throws InterruptedException {

		login = new Pay1Login();
		grade1 = new Add_Grade();

		login.Pay1_Login();

		login.Click_On_New_HR();

		grade1.Create_New_Grade();
		
		grade1.Delete_Grade();

	}

	@BeforeMethod
	public void InitializeBrowser() {
		launchApp();
	}

	@AfterMethod
//	@AfterTest
	public void tearDown() {
		driver.quit();
	}
	

}
