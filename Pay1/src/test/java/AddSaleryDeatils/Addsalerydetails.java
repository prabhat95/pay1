package AddSaleryDeatils;

import java.io.IOException;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Action.Action;
import BaseClass.Baseclass;
import PageObject.AddSalaryDetails;
import PageObject.Pay1Login;

public class Addsalerydetails extends Baseclass {
	AddSalaryDetails addsalerydetails;   
	Pay1Login pay1login;
       
       @BeforeTest(alwaysRun = true)
       public void launchBrowser() throws InterruptedException {	
    		launchApp();
}

  
       @Test (groups = {"Regression"})
       public void addsalerydetails1() throws IOException, InterruptedException {
    	   addsalerydetails = new AddSalaryDetails();
    	   pay1login = new Pay1Login();
    	   
    	  
    	   pay1login.Enteremail();
           Thread.sleep(1000);
           pay1login.enterPassword();
           Thread.sleep(1000);
           pay1login.clickSubmitButton();
           Thread.sleep(5000);
           addsalerydetails.clickNewHR1();
    	   Action.screencapture();
     	   Thread.sleep(1000);
    	   addsalerydetails.addsalerydetail1();
    	   Action.screencapture();
     	   Thread.sleep(1000);
    	   addsalerydetails.calender();
    	   Action.screencapture();
     	   Thread.sleep(1000);
    	   addsalerydetails.selectemployee();
    	   Action.screencapture();
     	   Thread.sleep(1000);
    	   addsalerydetails.searchemployee();
    	   Action.screencapture();
     	   Thread.sleep(1000);
    	   addsalerydetails.selectgrade();
    	   Action.screencapture();
     	   Thread.sleep(1000);
    	   addsalerydetails.enterCTC();
    	   Action.screencapture();
     	   Thread.sleep(1000);
    	   addsalerydetails.savebutton();
    	   addsalerydetails.successmessage1();
       }
    	   @AfterTest(alwaysRun = true)
           public void exit() {
        	   driver.quit();
           }}
    	   
  